﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laserball.Events;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Laserball.Figures
{
    public class Bomb : Figure
    {
        public event EventHandler<BombEvent> BombEvent;
        public Bomb(Position pos) : base(pos)
        {
        }

        public override void ActivateLaser(Figure figure)
        {
            if (Input == NeighborDirection.Undefined)
            {
                Input = GetDirection(figure);
            }
            else
            {
                Input2 = GetDirection(figure);
            }
            Status = FigureStatus.Active;
            BombEvent args = new BombEvent();
            OnBombEvent(args);
        }


        public override void Paint(int posX, int posY, int height, SpriteBatch spriteBatch, ContentManager content)
        {
            Texture = content.Load<Texture2D>("bomb");
            spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
            if (Status == FigureStatus.Active)
            {
                switch (Input)
                {
                    case NeighborDirection.Down:
                        spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.9), 0)), height, Convert.ToInt32(Math.Round(height * 0.1, 0))), Color.White);
                        break;
                    case NeighborDirection.Up:
                        spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.1, 0))), Color.White);
                        break;
                    case NeighborDirection.Left:
                        spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.1, 0)), height), Color.White);
                        break;
                    case NeighborDirection.Right:
                        spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.9), 0)), posY, Convert.ToInt32(Math.Round(height * 0.1, 0)), height), Color.White);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                if (Input2 != NeighborDirection.Undefined)
                {
                    switch (Input2)
                    {
                        case NeighborDirection.Down:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.9), 0)), height, Convert.ToInt32(Math.Round(height * 0.1, 0))), Color.White);
                            break;
                        case NeighborDirection.Up:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.1, 0))), Color.White);
                            break;
                        case NeighborDirection.Left:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.1, 0)), height), Color.White);
                            break;
                        case NeighborDirection.Right:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.9), 0)), posY, Convert.ToInt32(Math.Round(height * 0.1, 0)), height), Color.White);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
        }
        private void OnBombEvent(BombEvent args)
        {
            BombEvent?.Invoke(this, args);
        }
    }
}
