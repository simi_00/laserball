﻿using System.Collections.Generic;
using System.Diagnostics;
using Windows.Media.Devices.Core;
using Laserball.UWP.Figures;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Windows.UI.Core;
using Point = Microsoft.Xna.Framework.Point;

namespace Laserball.UWP
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        private bool IsScrollable;
        private int posX = 0;
        private int posY = 0;
        private int height = 0;
        private int diff;

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        private Playboard board = Playboard.GetInstance();
        private Figure curFigure;
        private Texture2D border;
        MouseState oldMouseState;
        private KeyboardState oldKeyboardState;
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            IsMouseVisible = true;
            SoundEffect song = Content.Load<SoundEffect>("lb");
            SoundEffectInstance soundEffectInstance = song.CreateInstance();
            soundEffectInstance.IsLooped = true;
            soundEffectInstance.Play();
            board.InitBoard(Content);
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            border = Content.Load<Texture2D>("border");
            // TODO: use this.Content to load your game content here

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            MouseState mouseState = Mouse.GetState();
            height = GraphicsDevice.PresentationParameters.Bounds.Height / board.Positions.GetLength(0);
            if (height * board.Positions.GetLength(1) > GraphicsDevice.PresentationParameters.Bounds.Width)
            {
                IsScrollable = true;
            }
            if (IsScrollable && mouseState.X > GraphicsDevice.PresentationParameters.Bounds.Width * 0.8 && posX * -1 < height * board.Positions.GetLength(1) - GraphicsDevice.PresentationParameters.Bounds.Width)
            {
                posX -= 10;
            }
            if (IsScrollable && mouseState.X < GraphicsDevice.PresentationParameters.Bounds.Width * 0.2 && posX < 0)
            {
                posX += 10;
            }
            if (mouseState.Y / height < board.Positions.GetLength(0) && ((mouseState.X - diff) / height) + ((posX * -1) / height) + 1 < board.Positions.GetLength(1))
            {
                curFigure = posX < 0 ? board.Positions[mouseState.Y / height, ((mouseState.X - diff) / height) + ((posX * -1) / height) + 1] : board.Positions[mouseState.Y / height, (mouseState.X) / height];
            }
            if (mouseState.LeftButton == ButtonState.Pressed && curFigure is Mirror && oldMouseState.LeftButton == ButtonState.Released)
            {
                if (posX < 0)
                {
                    ((Mirror)board.Positions[mouseState.Y / height, ((mouseState.X - diff) / height) + ((posX * -1) / height) + 1]).Direction = ((Mirror)curFigure).Direction == MirrorDirection.Forward ? MirrorDirection.Backward : MirrorDirection.Forward;
                }
                else
                {
                    ((Mirror)board.Positions[mouseState.Y / height, (mouseState.X / height)]).Direction = ((Mirror)curFigure).Direction == MirrorDirection.Forward ? MirrorDirection.Backward : MirrorDirection.Forward;
                }
            }

            KeyboardState keyboard = Keyboard.GetState();
            if (curFigure is Mirror)
            {
                if (keyboard.IsKeyDown(Keys.Down) && !oldKeyboardState.IsKeyDown(Keys.Down))
                {
                    while (board.Move(curFigure, curFigure.GetNeighbor(NeighborDirection.Down)))
                    {
                        Windows.UI.Xaml.Window.Current.CoreWindow.PointerPosition = new Windows.Foundation.Point(Windows.UI.Xaml.Window.Current.CoreWindow.PointerPosition.X, Windows.UI.Xaml.Window.Current.CoreWindow.PointerPosition.Y + height);
                        Mouse.SetPosition(mouseState.X,mouseState.Y+height);
                        mouseState = Mouse.GetState();
                        if (mouseState.Y / height < board.Positions.GetLength(0) && ((mouseState.X - diff) / height) + ((posX * -1) / height) + 1 < board.Positions.GetLength(1))
                        {
                            curFigure = posX < 0 ? board.Positions[mouseState.Y / height, ((mouseState.X - diff) / height) + ((posX * -1) / height) + 1] : board.Positions[mouseState.Y / height, (mouseState.X) / height];
                        }
                        Draw(gameTime);
                    }
                }
                if (keyboard.IsKeyDown(Keys.Up) && !oldKeyboardState.IsKeyDown(Keys.Up))
                {
                    while (board.Move(curFigure, curFigure.GetNeighbor(NeighborDirection.Up)))
                    {      
                        Windows.UI.Xaml.Window.Current.CoreWindow.PointerPosition = new Windows.Foundation.Point(Windows.UI.Xaml.Window.Current.CoreWindow.PointerPosition.X, Windows.UI.Xaml.Window.Current.CoreWindow.PointerPosition.Y - height);
                        Mouse.SetPosition(mouseState.X, mouseState.Y - height);
                        mouseState = Mouse.GetState();
                        if (mouseState.Y / height < board.Positions.GetLength(0) && ((mouseState.X - diff) / height) + ((posX * -1) / height) + 1 < board.Positions.GetLength(1))
                        {
                            curFigure = posX < 0 ? board.Positions[mouseState.Y / height, ((mouseState.X - diff) / height) + ((posX * -1) / height) + 1] : board.Positions[mouseState.Y / height, (mouseState.X) / height];
                        }
                        Draw(gameTime);

                    }
                }
                if (keyboard.IsKeyDown(Keys.Left) && !oldKeyboardState.IsKeyDown(Keys.Left))
                {
                    while (board.Move(curFigure, curFigure.GetNeighbor(NeighborDirection.Left)))
                    {
                        Windows.UI.Xaml.Window.Current.CoreWindow.PointerPosition = new Windows.Foundation.Point(Windows.UI.Xaml.Window.Current.CoreWindow.PointerPosition.X - height, Windows.UI.Xaml.Window.Current.CoreWindow.PointerPosition.Y);
                        Mouse.SetPosition(mouseState.X - height, mouseState.Y);
                        mouseState = Mouse.GetState();
                        if (mouseState.Y / height < board.Positions.GetLength(0) && ((mouseState.X - diff) / height) + ((posX * -1) / height) + 1 < board.Positions.GetLength(1))
                        {
                            curFigure = posX < 0 ? board.Positions[mouseState.Y / height, ((mouseState.X - diff) / height) + ((posX * -1) / height) + 1] : board.Positions[mouseState.Y / height, (mouseState.X) / height];
                        }
                        Draw(gameTime);

                    }
                }
                if (keyboard.IsKeyDown(Keys.Right) && !oldKeyboardState.IsKeyDown(Keys.Right))
                {
                    while (board.Move(curFigure, curFigure.GetNeighbor(NeighborDirection.Right)))
                    {
                        Windows.UI.Xaml.Window.Current.CoreWindow.PointerPosition = new Windows.Foundation.Point(Windows.UI.Xaml.Window.Current.CoreWindow.PointerPosition.X+height, Windows.UI.Xaml.Window.Current.CoreWindow.PointerPosition.Y);
                        Mouse.SetPosition(mouseState.X + height, mouseState.Y);
                        mouseState = Mouse.GetState();
                        if (mouseState.Y / height < board.Positions.GetLength(0) && ((mouseState.X - diff) / height) + ((posX * -1) / height) + 1 < board.Positions.GetLength(1))
                        {
                            curFigure = posX < 0 ? board.Positions[mouseState.Y / height, ((mouseState.X - diff) / height) + ((posX * -1) / height) + 1] : board.Positions[mouseState.Y / height, (mouseState.X) / height];
                        }
                        Draw(gameTime);

                    }
                } 
            }
            diff = (((posX * -1) - ((posX * -1) / height) * height));
            oldMouseState = mouseState;
            oldKeyboardState = keyboard;
            Debug.WriteLine("X: " + mouseState.X + " Y: " + mouseState.Y);
            base.Update(gameTime);
        }



        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            MouseState mouseState = Mouse.GetState();
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();
            board.Paint(posX, posY, height, spriteBatch, Content);
            spriteBatch.Draw(border,
                posX < 0
                    ? new Rectangle(((((mouseState.X - diff) / height) + 1) * height) - diff,
                        (mouseState.Y / height) * height, height, height)
                    : new Rectangle(((((mouseState.X) / height)) * height), (mouseState.Y / height) * height, height,
                        height), Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }
        //TODO
        //public int GetMousePositionX()
        //{
        //    return Windows.UI.Xaml.Window.Current.CoreWindow.PointerPosition.X - 
        //}
    }
}
