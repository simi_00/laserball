﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laserball.Figures;
using Newtonsoft.Json;
using PCLStorage;

namespace Laserball
{
    public static class LoadLevel
    {
        public static async Task<Figure[,]> LevelAsync(string level)
        {
            var root = FileSystem.Current.LocalStorage;
            var testFolderPath = await root.GetFolderAsync("LaserballLevel").ConfigureAwait(false);
            var outfile = await testFolderPath.GetFileAsync(level);
            string json = await outfile.ReadAllTextAsync();
            return JsonConvert.DeserializeObject<Figure[,]>(json,new JsonSerializerSettings() {TypeNameHandling = TypeNameHandling.All});
        }
    }
}
