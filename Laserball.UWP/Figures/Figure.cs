﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Laserball.UWP.Figures
{
    public enum FigureStatus { Active, Inactive }

    public abstract class Figure : IEquatable<Figure>

    {
        private Playboard _board;
        public FigureStatus Status { get; set; } = FigureStatus.Inactive;
        public NeighborDirection Output { get; set; }
        public Texture2D Texture { get; set; }
        public bool CanMove { get; set; } = false;
        public Playboard Board
        {
            get { return _board ?? (_board = Playboard.GetInstance()); }
        }

        public abstract void ActivateLaser(Figure figure);
        public Position Pos { get; set; }
        public Figure(Position pos)
        {
            Pos = pos;
        }

        public virtual void Paint(int posX, int posY, int height, SpriteBatch spriteBatch, ContentManager content)
        {
            if (Status == FigureStatus.Active)
            {
                if (Output == NeighborDirection.Down || Output == NeighborDirection.Up)
                {
                    spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, height), Color.White);
                }
                else if (Output == NeighborDirection.Left || Output == NeighborDirection.Right)
                {
                    spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, height, height), Color.White);
                }

            }
        }
        public bool Equals(Figure other)
        {
            return this.Pos.Equals(other.Pos);
        }
        public NeighborDirection GetDirection(Figure fig)
        {
            List<Neighbor> neighbors = Board.GetNeighbors(this);
            foreach (Neighbor neighbor in neighbors)
            {
                if (neighbor.Fig.Equals(fig))
                {
                    return neighbor.Direction;
                }
            }
            throw new Exception("No direction found");
        }

        public Neighbor GetNeighbor(NeighborDirection direction)
        {
            List<Neighbor> neighbors = Board.GetNeighbors(this);
            foreach (Neighbor neighbor in neighbors)
            {
                if (neighbor.Direction == direction)
                {
                    return neighbor;
                }
            }
            return null;
        }
        public virtual NeighborDirection GetOutputDirection(NeighborDirection inputDirection)
        {
            switch (inputDirection)
            {
                case NeighborDirection.Up:
                    return NeighborDirection.Down;
                case NeighborDirection.Right:
                    return NeighborDirection.Left;
                case NeighborDirection.Down:
                    return NeighborDirection.Up;
                case NeighborDirection.Left:
                    return NeighborDirection.Right;
                default:
                    throw new ArgumentOutOfRangeException(nameof(inputDirection), inputDirection, null);
            }
        }
    }

}
