﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LaserballConsole.Figures;

namespace LaserballConsole
{
    public class Laser:Figure
    {
        public Figure fig { get; set; }
        public Laser(Position pos) : base(pos)
        {
        }

        public override void ActivateLaser(Figure figure)
        {
            fig = figure;
            Neighbor nextNeighbor = GetNeighbor(NeighborDirection.Right);
            nextNeighbor.Fig.ActivateLaser(this);
        }

        public override void Paint()
        {
            Console.Write("L");
        }
        
    }
}
