﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Laserball.Figures
{
    public enum MirrorDirection
    {
        Forward, Backward
    }
    public class Mirror : Figure
    {
        public bool IsDestroyed { get; set; }
        public MirrorDirection Direction { get; set; } = MirrorDirection.Forward;
        public Mirror(Position pos) : base(pos)
        {
            CanMove = true;
        }
        public override void ActivateLaser(Figure figure)
        {
            if (Input == NeighborDirection.Undefined)
            {
                Input = GetDirection(figure);
                Output = GetOutputDirection(Input);
                Neighbor nextNeighbor = GetNeighbor(Output);
                if (nextNeighbor != null)
                {
                    Figure nextFigure = nextNeighbor.Fig;
                    nextFigure.ActivateLaser(this);
                }

            }
            else
            {
                Input2 = GetDirection(figure);
                Output2 = GetOutputDirection(Input2);
                Neighbor nextNeighbor = GetNeighbor(Output2);
                if (nextNeighbor != null)
                {
                    Figure nextFigure = nextNeighbor.Fig;
                    nextFigure.ActivateLaser(this);
                }
            }

            Status = FigureStatus.Active;
        }

        public override NeighborDirection GetOutputDirection(NeighborDirection inputDirection)
        {
            if (IsDestroyed == false)
            {
                if (Direction == MirrorDirection.Forward)
                {
                    switch (inputDirection)
                    {
                        case NeighborDirection.Up:
                            return NeighborDirection.Left;
                        case NeighborDirection.Right:
                            return NeighborDirection.Down;
                        case NeighborDirection.Down:
                            return NeighborDirection.Right;
                        case NeighborDirection.Left:
                            return NeighborDirection.Up;
                    }
                }
                else
                {
                    switch (inputDirection)
                    {
                        case NeighborDirection.Up:
                            return NeighborDirection.Right;
                        case NeighborDirection.Right:
                            return NeighborDirection.Up;
                        case NeighborDirection.Down:
                            return NeighborDirection.Left;
                        case NeighborDirection.Left:
                            return NeighborDirection.Down;
                    }
                }

            }
            else
            {
                switch (inputDirection)
                {
                    case NeighborDirection.Up:
                        return NeighborDirection.Down;
                    case NeighborDirection.Right:
                        return NeighborDirection.Left;
                    case NeighborDirection.Down:
                        return NeighborDirection.Up;
                    case NeighborDirection.Left:
                        return NeighborDirection.Right;
                }

            }

            return NeighborDirection.Undefined;
        }
        public override void Paint(int posX, int posY, int height, SpriteBatch spriteBatch, ContentManager content)
        {
            switch (Direction)
            {
                case MirrorDirection.Forward:
                    if (!IsNotMovable)
                    {
                        Texture = content.Load<Texture2D>("mirror-forward");
                    }
                    else if (IsNotMovable)
                    {
                        Texture = content.Load<Texture2D>("mirror-forward-des");
                    }
                    if (IsDestroyed)
                    {
                        Texture = content.Load<Texture2D>("mirror-destroyed");
                    }
                    break;
                case MirrorDirection.Backward:
                    if (!IsNotMovable)
                    {
                        Texture = content.Load<Texture2D>("mirror-backward");
                    }
                    else if (IsNotMovable)
                    {
                        Texture = content.Load<Texture2D>("mirror-backward-des");
                    }
                    if (IsDestroyed)
                    {
                        Texture = content.Load<Texture2D>("mirror-destroyed");
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
            if (Status == FigureStatus.Active)
            {
                switch (Output)
                {
                    case NeighborDirection.Down:
                        spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.55), 0)), height, Convert.ToInt32(Math.Round(height * 0.45, 0))), Color.White);
                        break;
                    case NeighborDirection.Up:
                        spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.45, 0))), Color.White);
                        break;
                    case NeighborDirection.Left:
                        spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.45, 0)), height), Color.White);
                        break;
                    case NeighborDirection.Right:
                        spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.55), 0)), posY, Convert.ToInt32(Math.Round(height * 0.45, 0)), height), Color.White);
                        break;
                }
                if (Output2 != NeighborDirection.Undefined)
                {
                    switch (Output2)
                    {
                        case NeighborDirection.Down:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.55), 0)), height, Convert.ToInt32(Math.Round(height * 0.45, 0))), Color.White);
                            break;
                        case NeighborDirection.Up:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.45, 0))), Color.White);
                            break;
                        case NeighborDirection.Left:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.45, 0)), height), Color.White);
                            break;
                        case NeighborDirection.Right:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.55), 0)), posY, Convert.ToInt32(Math.Round(height * 0.45, 0)), height), Color.White);
                            break;
                    }
                }
                switch (Input)
                {
                    case NeighborDirection.Down:
                        spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.55), 0)), height, Convert.ToInt32(Math.Round(height * 0.45, 0))), Color.White);
                        break;
                    case NeighborDirection.Up:
                        spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.45, 0))), Color.White);
                        break;
                    case NeighborDirection.Left:
                        spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.45, 0)), height), Color.White);
                        break;
                    case NeighborDirection.Right:
                        spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.55), 0)), posY, Convert.ToInt32(Math.Round(height * 0.45, 0)), height), Color.White);
                        break;
                }
                if (Input2 != NeighborDirection.Undefined)
                {
                    switch (Input2)
                    {
                        case NeighborDirection.Down:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.55), 0)), height, Convert.ToInt32(Math.Round(height * 0.45, 0))), Color.White);
                            break;
                        case NeighborDirection.Up:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.45, 0))), Color.White);
                            break;
                        case NeighborDirection.Left:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.45, 0)), height), Color.White);
                            break;
                        case NeighborDirection.Right:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.55), 0)), posY, Convert.ToInt32(Math.Round(height * 0.45, 0)), height), Color.White);
                            break;
                    }
                }
            }
        }
    }
}
