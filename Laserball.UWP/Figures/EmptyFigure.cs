﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Laserball.UWP.Figures
{
    public class EmptyFigure:Figure
    {
        public EmptyFigure(Position pos) : base(pos)
        {
            CanMove = true;
        }

        public override void ActivateLaser(Figure figure)
        {
            NeighborDirection input = GetDirection(figure);
            Output = GetOutputDirection(input);
            Neighbor nextNeighbor = GetNeighbor(Output);
            if (nextNeighbor != null)
            {
                Figure nextFigure = nextNeighbor.Fig;
                nextFigure.ActivateLaser(this);
            }
            Status = FigureStatus.Active;
        }


        public override void Paint(int posX, int posY, int height, SpriteBatch spriteBatch, ContentManager content)
        {
            Texture = content.Load<Texture2D>("empty");
            spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
            base.Paint(posX, posY, height,spriteBatch,content);
        }
    }
}
