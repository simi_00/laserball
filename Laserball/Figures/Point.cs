﻿using System;
using Laserball.Events;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Laserball.Figures
{
    public class Point : EmptyFigure
    {
        private bool _isActive;
        public event EventHandler<PointEvent> PointEvent;
        public bool Collected { get; set; } = false;
        public Point(Position pos) : base(pos)
        {
            CanMove = false;
        }

        public override void ActivateLaser(Figure figure)
        {
            if (Input == NeighborDirection.Undefined)
            {
                Input = GetDirection(figure);            
            }
            else
            {
                Input2 = GetDirection(figure);
            }
            Status = FigureStatus.Active;
            if (Collected)
            {
                base.ActivateLaser(figure);
            }
            else
            {
                PointEvent args = new PointEvent();
                OnPointEvent(args);
            }
        }

        public override void Paint(int posX, int posY, int height, SpriteBatch spriteBatch, ContentManager content)
        {
            if (Collected)
            {
                base.Paint(posX, posY, height, spriteBatch, content);
            }
            else
            {
                Texture = content.Load<Texture2D>("point");
                spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
                if (Status == FigureStatus.Active)
                {
                    switch (Input)
                    {
                        case NeighborDirection.Down:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.8), 0)), height, Convert.ToInt32(Math.Round(height * 0.2, 0))), Color.White);
                            break;
                        case NeighborDirection.Up:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.2, 0))), Color.White);
                            break;
                        case NeighborDirection.Left:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.2, 0)), height), Color.White);
                            break;
                        case NeighborDirection.Right:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.8), 0)), posY, Convert.ToInt32(Math.Round(height * 0.2, 0)), height), Color.White);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    if (Input2 != NeighborDirection.Undefined)
                    {
                        switch (Input2)
                        {
                            case NeighborDirection.Down:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.8), 0)), height, Convert.ToInt32(Math.Round(height * 0.2, 0))), Color.White);
                                break;
                            case NeighborDirection.Up:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.2, 0))), Color.White);
                                break;
                            case NeighborDirection.Left:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.2, 0)), height), Color.White);
                                break;
                            case NeighborDirection.Right:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.8), 0)), posY, Convert.ToInt32(Math.Round(height * 0.2, 0)), height), Color.White);
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }
                }
            }
        }
        protected virtual void OnPointEvent(PointEvent e)
        {
            PointEvent?.Invoke(this, e);
        }
    }
}
