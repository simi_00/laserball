﻿using System;
using System.Collections.Generic;
using LaserballConsole.Figures;

namespace LaserballConsole
{
    public class Playboard
    {
        public Figure[,] Positions { get; set; }
        private static Playboard _instance;
        public Laser LaserStart { get; set; }
        public static Playboard GetInstance()
        {
            return _instance ?? (_instance = new Playboard());
        }

        public void Paint()
        {
            ClearBoard();
            LaserStart.ActivateLaser(LaserStart);
            for (int y = 0; y < Positions.GetLength(0); y++)
            {
                Console.WriteLine();
                for (int x = 0; x < Positions.GetLength(1); x++)
                {

                    Figure fig = Positions[y, x];
                    fig.Paint();
                }
            }
        }

        public List<Neighbor> GetNeighbors(Figure fig)
        {
            List<Neighbor> neighbors = new List<Neighbor>();
            int x = fig.Pos.X;
            int y = fig.Pos.Y;
            //Pos Left
            if (x > 0)
            {
                Figure leftFigure = Positions[y, x - 1];
                neighbors.Add(new Neighbor() { Direction = NeighborDirection.Left, Fig = leftFigure });
            }
            //Pos Right
            if (x + 1 < Positions.GetLength(1))
            {
                Figure rightFigure = Positions[y, x + 1];
                neighbors.Add(new Neighbor() { Direction = NeighborDirection.Right, Fig = rightFigure });
            }
            //Pos Up
            if (y > 0)
            {
                Figure upFigure = Positions[y - 1, x];
                neighbors.Add(new Neighbor() { Direction = NeighborDirection.Up, Fig = upFigure });
            }
            //Pos Down
            if (y + 1 < Positions.GetLength(0))
            {
                Figure downFigure = Positions[y + 1, x];
                neighbors.Add(new Neighbor() { Direction = NeighborDirection.Down, Fig = downFigure });
            }
            return neighbors;
        }

        public void InitBoard()
        {
            //Positions = new Figure[10,7];
            //for (int y = 0; y < Positions.GetLength(0); y++)
            //{
            //    for (int x = 0; x < Positions.GetLength(1); x++)
            //    {
            //        Positions[y, x] = new EmptyFigure(new Position() {X=x, Y=y});
            //    }
            //}
            //Positions[3,4] = new Wall(new Position() {X=4, Y=3});
            //Positions[1,4] = new Border(new Position() {X=4,Y=1});
            //Positions[5, 4] = new Mirror(new Position() {X=4,Y=5});
            //Positions[5,0]= new Laser(new Position() {X=0,Y=5});
            //Positions[7,2] = new Door(new Position() {X = 2,Y=7});
            //Positions[7, 4] = new Mirror(new Position() { X = 4, Y = 7 });

            //LaserStart = (Laser) Positions[5, 0];
            Positions = new Figure[7, 10];
            Positions[0, 0] = new Border(new Position() { X = 0, Y = 0 });
            Positions[0, 1] = new Border(new Position() { X = 1, Y = 0 });
            Positions[0, 2] = new Border(new Position() { X = 2, Y = 0 });
            Positions[0, 3] = new Border(new Position() { X = 3, Y = 0 });
            Positions[0, 4] = new Border(new Position() { X = 4, Y = 0 });
            Positions[0, 5] = new Border(new Position() { X = 5, Y = 0 });
            Positions[0, 6] = new Border(new Position() { X = 6, Y = 0 });
            Positions[0, 7] = new Border(new Position() { X = 7, Y = 0 });
            Positions[0, 8] = new Border(new Position() { X = 8, Y = 0 });
            Positions[0, 9] = new Border(new Position() { X = 9, Y = 0 });
            Positions[1, 0] = new Border(new Position() { X = 0, Y = 1 });
            Positions[1, 1] = new Wall(new Position() { X = 1, Y = 1 });
            Positions[1, 2] = new Wall(new Position() { X = 2, Y = 1 });
            Positions[1, 3] = new Wall(new Position() { X = 3, Y = 1 });
            Positions[1, 4] = new Wall(new Position() { X = 4, Y = 1 });
            Positions[1, 5] = new Wall(new Position() { X = 5, Y = 1 });
            Positions[1, 6] = new Wall(new Position() { X = 6, Y = 1 });
            Positions[1, 7] = new Wall(new Position() { X = 7, Y = 1 });
            Positions[1, 8] = new Wall(new Position() { X = 8, Y = 1 });
            Positions[1, 9] = new Border(new Position() { X = 9, Y = 1 });
            Positions[2, 0] = new Border(new Position() { X = 0, Y = 2 });
            Positions[2, 1] = new Wall(new Position() { X = 1, Y = 2 });
            Positions[2, 2] = new EmptyFigure(new Position() { X = 2, Y = 2 });
            Positions[2, 3] = new EmptyFigure(new Position() { X = 3, Y = 2 });
            Positions[2, 4] = new EmptyFigure(new Position() { X = 4, Y = 2 });
            Positions[2, 5] = new EmptyFigure(new Position() { X = 5, Y = 2 });
            Positions[2, 6] = new EmptyFigure(new Position() { X = 6, Y = 2 });
            Positions[2, 7] = new Mirror(new Position() { X = 7, Y = 2 });
            Positions[2, 8] = new Goal(new Position() { X = 8, Y = 2 });
            Positions[2, 9] = new Border(new Position() { X = 9, Y = 2 });
            Positions[3, 0] = new Border(new Position() { X = 0, Y = 3 });
            Positions[3, 1] = new Wall(new Position() { X = 1, Y = 3 });
            Positions[3, 2] = new Wall(new Position() { X = 2, Y = 3 });
            Positions[3, 3] = new Wall(new Position() { X = 3, Y = 3 });
            Positions[3, 4] = new Wall(new Position() { X = 4, Y = 3 });
            Positions[3, 5] = new Wall(new Position() { X = 5, Y = 3 });
            Positions[3, 6] = new Wall(new Position() { X = 6, Y = 3 });
            Positions[3, 7] = new Wall(new Position() { X = 7, Y = 3 });
            Positions[3, 8] = new Wall(new Position() { X = 8, Y = 3 });
            Positions[3, 9] = new Border(new Position() { X = 9, Y = 3 });
            Positions[4, 0] = new Laser(new Position() { X = 0, Y = 4 });
            Positions[4, 1] = new EmptyFigure(new Position() { X = 1, Y = 4 });
            Positions[4, 2] = new EmptyFigure(new Position() { X = 2, Y = 4 });
            Positions[4, 3] = new EmptyFigure(new Position() { X = 3, Y = 4 });
            Positions[4, 4] = new EmptyFigure(new Position() { X = 4, Y = 4 });
            Positions[4, 5] = new EmptyFigure(new Position() { X = 5, Y = 4 });
            Positions[4, 6] = new Wall(new Position() { X = 6, Y = 4 });
            Positions[4, 7] = new Wall(new Position() { X = 7, Y = 4 });
            Positions[4, 8] = new Wall(new Position() { X = 8, Y = 4 });
            Positions[4, 9] = new Wall(new Position() { X = 9, Y = 4 });
            Positions[5, 0] = new Border(new Position() { X = 0, Y = 5 });
            Positions[5, 1] = new Mirror(new Position() { X = 1, Y = 5 });
            Positions[5, 2] = new EmptyFigure(new Position() { X = 2, Y = 5 });
            Positions[5, 3] = new EmptyFigure(new Position() { X = 3, Y = 5 });
            Positions[5, 4] = new Point(new Position() { X = 4, Y = 5 });
            Positions[5, 5] = new EmptyFigure(new Position() { X = 5, Y = 5 });
            Positions[5, 6] = new Wall(new Position() { X = 6, Y = 5 });
            Positions[5, 7] = new Wall(new Position() { X = 7, Y = 5 });
            Positions[5, 8] = new Wall(new Position() { X = 8, Y = 5 });
            Positions[5, 9] = new Border(new Position() { X = 9, Y = 5 });
            Positions[6, 0] = new Border(new Position() { X = 0, Y = 0 });
            Positions[6, 1] = new Border(new Position() { X = 1, Y = 0 });
            Positions[6, 2] = new Border(new Position() { X = 2, Y = 0 });
            Positions[6, 3] = new Border(new Position() { X = 3, Y = 0 });
            Positions[6, 4] = new Border(new Position() { X = 4, Y = 0 });
            Positions[6, 5] = new Border(new Position() { X = 5, Y = 0 });
            Positions[6, 6] = new Border(new Position() { X = 6, Y = 0 });
            Positions[6, 7] = new Border(new Position() { X = 7, Y = 0 });
            Positions[6, 8] = new Border(new Position() { X = 8, Y = 0 });
            Positions[6, 9] = new Border(new Position() { X = 9, Y = 0 });
            LaserStart = (Laser)Positions[4, 0];
        }

        public void ClearBoard()
        {
            for (int y = 0; y < Positions.GetLength(0); y++)
            {
                for (int x = 0; x < Positions.GetLength(1); x++)
                {

                    Figure fig = Positions[y, x];
                    fig.Status = FigureStatus.Inactive;
                }
            }
        }
        private Playboard()
        {

        }
    }
}
