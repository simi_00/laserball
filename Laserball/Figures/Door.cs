﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Laserball.Events;

namespace Laserball.Figures
{
    public enum DoorDirection
    {
        Vertical, Horizontal
    }

    public enum DoorStatus
    {
        Open, Closed
    }
    public class Door : Figure
    {
        public event EventHandler<DoorEvent> DoorEvent;
        public DoorDirection Direction { get; set; } = DoorDirection.Horizontal;
        public DoorStatus DoorPosition { get; set; } = DoorStatus.Closed;
        public Door(Position pos) : base(pos)
        {
        }

        public override void ActivateLaser(Figure figure)
        {
            if (Input == NeighborDirection.Undefined)
            {
                Input = GetDirection(figure);
            }
            else
            {
                Input2 = GetDirection(figure);
            }
            if (((Input == NeighborDirection.Down || Input == NeighborDirection.Up) && Direction == DoorDirection.Horizontal) || ((Input == NeighborDirection.Right || Input == NeighborDirection.Left) && Direction == DoorDirection.Vertical))
            {
                DoorEvent args = new DoorEvent();
                OnDoorEvent(args);
            }
            if ((((Input == NeighborDirection.Left || Input == NeighborDirection.Right) && Direction == DoorDirection.Horizontal) || ((Input == NeighborDirection.Down || Input == NeighborDirection.Up) && Direction == DoorDirection.Vertical)) && DoorPosition == DoorStatus.Open)
            {
                Output = GetOutputDirection(Input);
                Neighbor nextNeighbor = GetNeighbor(Output);
                if (nextNeighbor != null)
                {
                    Figure nextFigure = nextNeighbor.Fig;
                    nextFigure.ActivateLaser(this);
                }
            }
            if (((Input2 == NeighborDirection.Down || Input2 == NeighborDirection.Up) && Direction == DoorDirection.Horizontal) || ((Input2 == NeighborDirection.Right || Input2 == NeighborDirection.Left) && Direction == DoorDirection.Vertical))
            {
                DoorEvent args = new DoorEvent();
                OnDoorEvent(args);
            }
            if ((((Input2 == NeighborDirection.Left || Input2 == NeighborDirection.Right) && Direction == DoorDirection.Horizontal) || ((Input2 == NeighborDirection.Down || Input2 == NeighborDirection.Up) && Direction == DoorDirection.Vertical)) && DoorPosition == DoorStatus.Open)
            {
                Output = GetOutputDirection(Input);
                Neighbor nextNeighbor = GetNeighbor(Output);
                if (nextNeighbor != null)
                {
                    Figure nextFigure = nextNeighbor.Fig;
                    nextFigure.ActivateLaser(this);
                }
            }
            Status = FigureStatus.Active;
        }

        public override void Paint(int posX, int posY, int height, SpriteBatch spriteBatch, ContentManager content)
        {
            switch (Direction)
            {
                case DoorDirection.Vertical:
                    switch (DoorPosition)
                    {
                        case DoorStatus.Open:
                            Texture = content.Load<Texture2D>("door-ver-open");

                            break;
                        case DoorStatus.Closed:
                            Texture = content.Load<Texture2D>("door-ver-closed");
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    break;
                case DoorDirection.Horizontal:
                    switch (DoorPosition)
                    {
                        case DoorStatus.Open:
                            Texture = content.Load<Texture2D>("door-hor-open");
                            break;
                        case DoorStatus.Closed:
                            Texture = content.Load<Texture2D>("door-hor-closed");
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
            if (Status == FigureStatus.Active)
            {
                if (DoorPosition == DoorStatus.Closed)
                {
                    switch (Input)
                    {
                        case NeighborDirection.Down:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.85), 0)), height, Convert.ToInt32(Math.Round(height * 0.15, 0))), Color.White);
                            break;
                        case NeighborDirection.Up:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.15, 0))), Color.White);
                            break;
                        case NeighborDirection.Left:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.15, 0)), height), Color.White);
                            break;
                        case NeighborDirection.Right:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.85), 0)), posY, Convert.ToInt32(Math.Round(height * 0.15, 0)), height), Color.White);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    if (Input2 != NeighborDirection.Undefined)
                    {
                        switch (Input2)
                        {
                            case NeighborDirection.Down:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.85), 0)), height, Convert.ToInt32(Math.Round(height * 0.15, 0))), Color.White);
                                break;
                            case NeighborDirection.Up:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.15, 0))), Color.White);
                                break;
                            case NeighborDirection.Left:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.15, 0)), height), Color.White);
                                break;
                            case NeighborDirection.Right:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.85), 0)), posY, Convert.ToInt32(Math.Round(height * 0.15, 0)), height), Color.White);
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }
                }
                else
                {
                    switch (Direction)
                    {
                        case DoorDirection.Vertical:
                            switch (Input)
                            {
                                case NeighborDirection.Down:
                                    base.Paint(posX, posY, height, spriteBatch, content);
                                    break;
                                case NeighborDirection.Up:
                                    base.Paint(posX, posY, height, spriteBatch, content);
                                    break;
                                case NeighborDirection.Left:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.15, 0)), height), Color.White);
                                    break;
                                case NeighborDirection.Right:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.85), 0)), posY, Convert.ToInt32(Math.Round(height * 0.15, 0)), height), Color.White);
                                    break;
                                default:
                                    throw new ArgumentOutOfRangeException();
                            }
                            if (Input2 != NeighborDirection.Undefined)
                            {
                                switch (Input2)
                                {
                                    case NeighborDirection.Down:
                                        base.Paint(posX, posY, height, spriteBatch, content);
                                        break;
                                    case NeighborDirection.Up:
                                        base.Paint(posX, posY, height, spriteBatch, content);
                                        break;
                                    case NeighborDirection.Left:
                                        spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.15, 0)), height), Color.White);
                                        break;
                                    case NeighborDirection.Right:
                                        spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.85), 0)), posY, Convert.ToInt32(Math.Round(height * 0.15, 0)), height), Color.White);
                                        break;
                                    default:
                                        throw new ArgumentOutOfRangeException();
                                }
                            }
                            break;
                        case DoorDirection.Horizontal:
                            switch (Input)
                            {
                                case NeighborDirection.Down:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.9), 0)), height, Convert.ToInt32(Math.Round(height * 0.1, 0))), Color.White);
                                    break;
                                case NeighborDirection.Up:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.1, 0))), Color.White);
                                    break;
                                case NeighborDirection.Left:
                                    base.Paint(posX, posY, height, spriteBatch, content);
                                    break;
                                case NeighborDirection.Right:
                                    base.Paint(posX, posY, height, spriteBatch, content);
                                    break;
                                default:
                                    throw new ArgumentOutOfRangeException();
                            }
                            if (Input2 != NeighborDirection.Undefined)
                            {
                                switch (Input2)
                                {
                                    case NeighborDirection.Down:
                                        spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.9), 0)), height, Convert.ToInt32(Math.Round(height * 0.1, 0))), Color.White);
                                        break;
                                    case NeighborDirection.Up:
                                        spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.1, 0))), Color.White);
                                        break;
                                    case NeighborDirection.Left:
                                        base.Paint(posX, posY, height, spriteBatch, content);
                                        break;
                                    case NeighborDirection.Right:
                                        base.Paint(posX, posY, height, spriteBatch, content);
                                        break;
                                    default:
                                        throw new ArgumentOutOfRangeException();
                                }
                            }
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
        }
        protected virtual void OnDoorEvent(DoorEvent e)
        {
            DoorEvent?.Invoke(this, e);
        }
    }
}
