﻿using System;
using System.Collections.Generic;

namespace LaserballConsole.Figures
{
    public enum FigureStatus { Active, Inactive }

    public abstract class Figure : IEquatable<Figure>

    {
        private Playboard _board;
        public FigureStatus Status { get; set; } = FigureStatus.Inactive;

        public Playboard Board
        {
            get { return _board ?? (_board = Playboard.GetInstance()); }
        }

        public abstract void ActivateLaser(Figure figure);
        public Position Pos { get; set; }

        public Figure(Position pos)
        {
            Pos = pos;
        }

        public abstract void Paint();
        public bool Equals(Figure other)
        {
            return this.Pos.Equals(other.Pos);
        }
        public NeighborDirection GetDirection(Figure fig)
        {
            List<Neighbor> neighbors = Board.GetNeighbors(this);
            foreach (Neighbor neighbor in neighbors)
            {
                if (neighbor.Fig.Equals(fig))
                {
                    return neighbor.Direction;
                }
            }
            throw new Exception("No direction found");
        }

        public Neighbor GetNeighbor(NeighborDirection direction)
        {
            List<Neighbor> neighbors = Board.GetNeighbors(this);
            foreach (Neighbor neighbor in neighbors)
            {
                if (neighbor.Direction == direction)
                {
                    return neighbor;
                }
            }
            return null;
        }
    }

}
