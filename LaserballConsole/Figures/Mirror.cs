﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserballConsole.Figures
{
    public enum MirrorDirection
    {
       Forward, Backward
    }
    public class Mirror:Figure
    {
        public  MirrorDirection Direction { get; set; } = MirrorDirection.Forward;
        public Mirror(Position pos) : base(pos)
        {
        }

        public override void ActivateLaser(Figure figure)
        {
            NeighborDirection input = GetDirection(figure);
            NeighborDirection output = GetOutputDirection(input);
            Neighbor nextNeighbor = GetNeighbor(output);
            if (nextNeighbor != null)
            {
                Figure nextFigure = nextNeighbor.Fig;
                nextFigure.ActivateLaser(this);
            }
            Status = FigureStatus.Active;
        }

        public override void Paint()
        {
            Console.Write(Direction == MirrorDirection.Forward ? "/" : @"\");
        }
        public NeighborDirection GetOutputDirection(NeighborDirection inputDirection)
        {

            if (Direction == MirrorDirection.Forward)
            {
                switch (inputDirection)
                {
                    case NeighborDirection.Up:
                        return NeighborDirection.Left;
                    case NeighborDirection.Right:
                        return NeighborDirection.Down;
                    case NeighborDirection.Down:
                        return NeighborDirection.Right;
                    case NeighborDirection.Left:
                        return NeighborDirection.Up;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(inputDirection), inputDirection, null);
                }
            }
            else
            {
                switch (inputDirection)
                {
                    case NeighborDirection.Up:
                        return NeighborDirection.Right;
                    case NeighborDirection.Right:
                        return NeighborDirection.Up;
                    case NeighborDirection.Down:
                        return NeighborDirection.Left;
                    case NeighborDirection.Left:
                        return NeighborDirection.Down;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(inputDirection), inputDirection, null);
                }
            }
        }
    }
}
