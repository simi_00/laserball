﻿namespace Laserball.UWP.Figures
{
    public enum NeighborDirection
    {
        Up, Right,Down,Left
    }
    public class Neighbor
    {
        public Figure Fig { get; set; }
        public NeighborDirection Direction { get; set; }
    }
}