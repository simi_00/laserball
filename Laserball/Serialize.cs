﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laserball.Figures;
using Newtonsoft.Json;
using PCLStorage;

namespace Laserball
{
    public static class Serialize
    {
        public static async Task SerializeLevelAsync(string json, string level)
        {
            var root = FileSystem.Current.LocalStorage;
            var testFolderPath = await root.CreateFolderAsync("LaserballLevel", CreationCollisionOption.OpenIfExists).ConfigureAwait(false);
            var outfile = await testFolderPath.CreateFileAsync(level, CreationCollisionOption.OpenIfExists);
            await outfile.WriteAllTextAsync(json);
        }
    }
}
