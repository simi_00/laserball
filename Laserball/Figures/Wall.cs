﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Laserball.Figures
{
    public class Wall : Figure
    {
        public Wall(Position pos) : base(pos)
        {
        }

        public override void ActivateLaser(Figure figure)
        {
            if (Input == NeighborDirection.Undefined)
            {
                Input = GetDirection(figure);
            }
            else
            {
                Input2 = GetDirection(figure);
            }
            Output = GetOutputDirection(Input);
            Neighbor nextNeighbor = GetNeighbor(Output);
            if (nextNeighbor != null)
            {
                Figure nextFigure = nextNeighbor.Fig;
                nextFigure.ActivateLaser(this);
            }
            Status = FigureStatus.Active;
        }

        public override void Paint(int posX, int posY, int height, SpriteBatch spriteBatch, ContentManager content)
        {
            Texture = content.Load<Texture2D>("wall");
            spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
            base.Paint(posX, posY, height, spriteBatch, content);
        }
    }
}
