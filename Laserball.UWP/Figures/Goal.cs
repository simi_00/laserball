﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Laserball.UWP.Figures
{
    public class Goal:Figure
    {
        public Goal(Position pos) : base(pos)
        {
           
        }

        public override void ActivateLaser(Figure figure)
        {
           
        }

        public override void Paint(int posX, int posY, int height, SpriteBatch spriteBatch, ContentManager content)
        {
            Texture = content.Load<Texture2D>("goal");
            spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
        }
    }
}
