﻿using System;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Laserball.UWP.Figures
{
    public enum DoorDirection
    {
        Vertical, Horizontal
    }

    public enum DoorStatus
    {
        Open, Closed
    }
    public class Door:Figure
    {
        public DoorDirection Direction { get; set; }
        public DoorStatus DoorPosition { get; set; } = DoorStatus.Closed;
        public Door(Position pos) : base(pos)
        {
        }

        public override void ActivateLaser(Figure figure)
        {
                NeighborDirection input = GetDirection(figure);
                if (((input == NeighborDirection.Down || input == NeighborDirection.Up) && Direction == DoorDirection.Horizontal) || ((input == NeighborDirection.Right || input == NeighborDirection.Left) && Direction == DoorDirection.Vertical))
                {
                   DoorPosition = DoorStatus.Open; 
                }
            if ((((input == NeighborDirection.Left || input == NeighborDirection.Right) && Direction == DoorDirection.Horizontal) || ((input == NeighborDirection.Down || input == NeighborDirection.Up) && Direction == DoorDirection.Vertical))&& DoorPosition == DoorStatus.Open)
            {
                Output = GetOutputDirection(input);
                Neighbor nextNeighbor = GetNeighbor(Output);
                if (nextNeighbor != null)
                {
                    Figure nextFigure = nextNeighbor.Fig;
                    nextFigure.ActivateLaser(this);
                }
                Status = FigureStatus.Active;
            }
        }

        public override void Paint(int posX, int posY, int height, SpriteBatch spriteBatch, ContentManager content)
        {
            
        }
    }
}
