﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using System.Web.Script.Services;
using System.Web.UI;
using Laserball.Figures;
using Newtonsoft.Json;

namespace LaserballService.Controllers
{
    public class LevelController : ApiController
    {
        public Figure[,] GetLevel(int level)
        {
            Configuration.Formatters.JsonFormatter.SerializerSettings.TypeNameHandling = TypeNameHandling.All;
            Figure[,] positions;
            string path = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, "Level", "Level" + level);
            positions = JsonConvert.DeserializeObject<Figure[,]>(File.ReadAllText(path),new JsonSerializerSettings() {TypeNameHandling = TypeNameHandling.All});
            return positions;
        }
    }
}
