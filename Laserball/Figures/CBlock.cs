﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laserball.Events;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;


namespace Laserball.Figures
{
    public enum CBlockMode
    {
        Stopping, Destroying
    }
    public class CBlock : Figure
    {
        public CBlockMode CBlockMode { get; set; } = CBlockMode.Stopping;
        public Mirror Mirror { get; set; }
        public event EventHandler<CBlockEvent> CBlockEvent;
        public int CBlockStatus { get; set; }
        public CBlock(Position pos) : base(pos)
        {
        }

        public override void ActivateLaser(Figure figure)
        {
            if (Input == NeighborDirection.Undefined)
            {
                Input = GetDirection(figure);
                if (CBlockStatus == 5)
                {
                    Output = GetOutputDirection(Input);
                }
            }
            else
            {
                Input2 = GetDirection(figure);
                if (CBlockStatus == 5)
                {
                    Output = GetOutputDirection(Input2);
                }
            }
            Neighbor nextNeighbor = GetNeighbor(Output);
            if (nextNeighbor != null)
            {
                Figure nextFigure = nextNeighbor.Fig;
                nextFigure.ActivateLaser(this);
            }
            Status = FigureStatus.Active;
            if (CBlockStatus < 5)
            {
                Mirror = Board.GetMirror(this);
                if (Mirror.IsNotMovable && CBlockMode == CBlockMode.Stopping)
                {
                    Board.Mirrors.Remove(Mirror);
                    Mirror = Board.GetMirror(this);
                }
                CBlockEvent args = new CBlockEvent();
                OnCBlockEvent(args);
            }
            
            if (CBlockStatus == 5)
            {
                if (Mirror != null)
                {
                    if (CBlockMode == CBlockMode.Stopping)
                    {
                        Mirror.IsNotMovable = true;
                    }
                    else
                    {
                        Mirror.IsDestroyed = true;
                        Mirror.IsNotMovable = true;
                    }
                }
                Mirror = null;
            }
        }

        public override void Paint(int posX, int posY, int height, SpriteBatch spriteBatch, ContentManager content)
        {
            switch (CBlockStatus)
            {
                case 0:
                    Texture = content.Load<Texture2D>("CBlock");
                    break;
                case 1:
                    Texture = content.Load<Texture2D>("CBlock-1");
                    break;
                case 2:
                    Texture = content.Load<Texture2D>("CBlock-2");
                    break;
                case 3:
                    Texture = content.Load<Texture2D>("CBlock-3");
                    break;
                case 4:
                    Texture = content.Load<Texture2D>("CBlock-4");
                    break;
                case 5:
                    Texture = content.Load<Texture2D>("CBlock-5");
                    break;
            }
            spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
            if (Status == FigureStatus.Active)
            {
                if (CBlockStatus < 4)
                {
                    switch (Input)
                    {
                        case NeighborDirection.Up:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.25, 0))), Color.White);
                            break;
                        case NeighborDirection.Left:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.25, 0)), height), Color.White);
                            break;
                        case NeighborDirection.Right:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-hor"),
                                new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.75), 0)), posY,
                                    Convert.ToInt32(Math.Round(height * 0.25, 0)), height), Color.White);
                            break;
                        case NeighborDirection.Down:
                            spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.75), 0)), height, Convert.ToInt32(Math.Round(height * 0.25, 0))), Color.White);
                            break;
                    }
                    if (Input2 != NeighborDirection.Undefined)
                    {
                        switch (Input2)
                        {
                            case NeighborDirection.Up:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.25, 0))), Color.White);
                                break;
                            case NeighborDirection.Left:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.25, 0)), height), Color.White);
                                break;
                            case NeighborDirection.Right:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"),
                                    new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.75), 0)), posY,
                                        Convert.ToInt32(Math.Round(height * 0.25, 0)), height), Color.White);
                                break;
                            case NeighborDirection.Down:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.75), 0)), height, Convert.ToInt32(Math.Round(height * 0.25, 0))), Color.White);
                                break;
                        }
                    }
                }
                else
                {
                    base.Paint(posX, posY, height, spriteBatch, content);
                }
            }
        }
        protected virtual void OnCBlockEvent(CBlockEvent e)
        {
            CBlockEvent?.Invoke(this, e);
        }
    }
}
