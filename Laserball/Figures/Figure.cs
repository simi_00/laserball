﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Laserball.Figures
{
    public enum FigureStatus { Active, Inactive }

    public abstract class Figure : IEquatable<Figure>

    {
        private Playboard _board;
        public FigureStatus Status { get; set; } = FigureStatus.Inactive;
        public NeighborDirection Output { get; set; }
        public NeighborDirection Output2 { get; set; }
        public NeighborDirection Input { get; set; }
        public NeighborDirection Input2 { get; set; }
        public Texture2D Texture { get; set; }
        public bool CanMove { get; set; } = false;
        public bool IsNotMovable{ get; set; } = false;
        public Playboard Board
        {
            get { return _board ?? (_board = Playboard.GetInstance()); }
        }

        public abstract void ActivateLaser(Figure figure);
        public Position Pos { get; set; }
        public Figure(Position pos)
        {
            Pos = pos;
        }

        public virtual void Paint(int posX, int posY, int height, SpriteBatch spriteBatch, ContentManager content)
        {
            if (Status == FigureStatus.Active)
            {
                if (Input == NeighborDirection.Down || Input == NeighborDirection.Up)
                {
                    spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, height), Color.White);
                }
                else if (Input == NeighborDirection.Left || Input == NeighborDirection.Right)
                {
                    spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, height, height), Color.White);
                }
                if (Input2 != NeighborDirection.Undefined)
                {
                    if (Input2 == NeighborDirection.Down || Input2 == NeighborDirection.Up)
                    {
                        spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, height), Color.White);
                    }
                    else if (Input2 == NeighborDirection.Left || Input2 == NeighborDirection.Right)
                    {
                        spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, height, height), Color.White);
                    }
                }

            }
        }
        public bool Equals(Figure other)
        {
            return this.Pos.Equals(other.Pos);
        }
        public NeighborDirection GetDirection(Figure fig)
        {
            List<Neighbor> neighbors = Board.GetNeighbors(this);
            foreach (Neighbor neighbor in neighbors)
            {
                if (neighbor.Fig.Equals(fig))
                {
                    return neighbor.Direction;
                }
            }
            throw new Exception("No direction found");
        }

        public Neighbor GetNeighbor(NeighborDirection direction)
        {
            List<Neighbor> neighbors = Board.GetNeighbors(this);
            foreach (Neighbor neighbor in neighbors)
            {
                if (neighbor.Direction == direction && !(neighbor.Fig is Laser))
                {
                    return neighbor;
                }
            }
            return null;
        }
        public virtual NeighborDirection GetOutputDirection(NeighborDirection inputDirection)
        {
            switch (inputDirection)
            {
                case NeighborDirection.Up:
                    return NeighborDirection.Down;
                case NeighborDirection.Right:
                    return NeighborDirection.Left;
                case NeighborDirection.Down:
                    return NeighborDirection.Up;
                case NeighborDirection.Left:
                    return NeighborDirection.Right;
                default:
                    throw new ArgumentOutOfRangeException(nameof(inputDirection), inputDirection, null);
            }
        }
    }

}
