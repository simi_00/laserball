﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserballConsole.Figures
{
    public class Goal:Figure
    {
        public Goal(Position pos) : base(pos)
        {
        }

        public override void ActivateLaser(Figure figure)
        {
           
        }

        public override void Paint()
        {
            Console.Write("G");
        }
    }
}
