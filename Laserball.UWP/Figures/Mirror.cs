﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Laserball.UWP.Figures
{
    public enum MirrorDirection
    {
       Forward, Backward
    }
    public class Mirror:Figure
    {
        private NeighborDirection input;
        public  MirrorDirection Direction { get; set; } = MirrorDirection.Forward;
        public Mirror(Position pos) : base(pos)
        {
            CanMove = true;
        }
        public override void ActivateLaser(Figure figure)
        {
            input = GetDirection(figure);
            Output = GetOutputDirection(input);
            Neighbor nextNeighbor = GetNeighbor(Output);
            if (nextNeighbor != null)
            {
                Figure nextFigure = nextNeighbor.Fig;
                nextFigure.ActivateLaser(this);
            }
            Status = FigureStatus.Active;
        }

        public override NeighborDirection GetOutputDirection(NeighborDirection inputDirection)
        {

            if (Direction == MirrorDirection.Forward)
            {
                switch (inputDirection)
                {
                    case NeighborDirection.Up:
                        return NeighborDirection.Left;
                    case NeighborDirection.Right:
                        return NeighborDirection.Down;
                    case NeighborDirection.Down:
                        return NeighborDirection.Right;
                    case NeighborDirection.Left:
                        return NeighborDirection.Up;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(inputDirection), inputDirection, null);
                }
            }
            else
            {
                switch (inputDirection)
                {
                    case NeighborDirection.Up:
                        return NeighborDirection.Right;
                    case NeighborDirection.Right:
                        return NeighborDirection.Up;
                    case NeighborDirection.Down:
                        return NeighborDirection.Left;
                    case NeighborDirection.Left:
                        return NeighborDirection.Down;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(inputDirection), inputDirection, null);
                }
            }
        }
        public override void Paint(int posX, int posY, int height, SpriteBatch spriteBatch, ContentManager content)
        {
            switch (Direction)
            {
                case MirrorDirection.Forward:
                    Texture = content.Load<Texture2D>("mirror-forward");
                    break;
                case MirrorDirection.Backward:
                    Texture = content.Load<Texture2D>("mirror-backward");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
            if (Status == FigureStatus.Active)
            {
                switch (Output)
                {
                    case NeighborDirection.Down:
                        spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY+(height*0.55),0)),height, Convert.ToInt32(Math.Round(height*0.45,0))), Color.White);
                        break;
                    case NeighborDirection.Up:
                        spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY,height, Convert.ToInt32(Math.Round(height*0.45,0))), Color.White);
                        break;
                    case NeighborDirection.Left:
                        spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height*0.45,0)), height), Color.White);
                        break;
                    case NeighborDirection.Right:
                        spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.55), 0)), posY, Convert.ToInt32(Math.Round(height*0.45,0)), height), Color.White);
                        break;
                }
                switch (input)
                {
                    case NeighborDirection.Down:
                        spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.55), 0)), height, Convert.ToInt32(Math.Round(height * 0.45, 0))), Color.White);
                        break;
                    case NeighborDirection.Up:
                        spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.45, 0))), Color.White);
                        break;
                    case NeighborDirection.Left:
                        spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.45, 0)), height), Color.White);
                        break;
                    case NeighborDirection.Right:
                        spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.55), 0)), posY, Convert.ToInt32(Math.Round(height * 0.45, 0)), height), Color.White);
                        break;
                }
            }
        }
    }
}
