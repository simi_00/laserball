﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Laserball.Figures
{
    public class EmptyFigure : Figure
    {
        public EmptyFigure(Position pos) : base(pos)
        {
            CanMove = true;
        }

        public override void ActivateLaser(Figure figure)
        {
            if (Input == NeighborDirection.Undefined)
            {
                Input = GetDirection(figure);
                Output = GetOutputDirection(Input);
                Neighbor nextNeighbor = GetNeighbor(Output);
                if (nextNeighbor != null)
                {
                    Figure nextFigure = nextNeighbor.Fig;
                    nextFigure.ActivateLaser(this);
                }
            }
            else
            {
                Input2 = GetDirection(figure);
                Output = GetOutputDirection(Input2);
                Neighbor nextNeighbor = GetNeighbor(Output);
                if (nextNeighbor != null)
                {
                    Figure nextFigure = nextNeighbor.Fig;
                    nextFigure.ActivateLaser(this);
                }
            }
            Status = FigureStatus.Active;
        }


        public override void Paint(int posX, int posY, int height, SpriteBatch spriteBatch, ContentManager content)
        {
            Texture = content.Load<Texture2D>("empty");
            spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
            if (Status == FigureStatus.Active)
            {
                if (Input == NeighborDirection.Down || Input == NeighborDirection.Up)
                {
                    spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, height), Color.White);
                }
                else if (Input == NeighborDirection.Left || Input == NeighborDirection.Right)
                {
                    spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, height, height), Color.White);
                }
                if (Input2 != NeighborDirection.Undefined)
                {
                    if (Input2 == NeighborDirection.Down || Input2 == NeighborDirection.Up)
                    {
                        spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, height), Color.White);
                    }
                    else if (Input2 == NeighborDirection.Left || Input2 == NeighborDirection.Right)
                    {
                        spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, height, height), Color.White);
                    }
                }
            }
            //base.Paint(posX, posY, height, spriteBatch, content);
        }
    }
}
