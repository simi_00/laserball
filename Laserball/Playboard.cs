﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Laserball.Events;
using Laserball.Figures;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Newtonsoft.Json;
using PCLStorage;
using Point = Laserball.Figures.Point;

namespace Laserball
{
    public enum GameState
    {
        Loading, Playing, GameOver
    }

    public class Playboard
    {
        public event EventHandler<LevelLoadedEvent> LevelLoadedEvent;
        public Figure[,] Positions { get; set; }
        public bool IsMoving { get; set; } = false;
        private static Playboard _instance;
        public Laser LaserStart { get; set; }
        public Goal Goal { get; set; }
        public GameState State { get; set; }
        public List<Mirror> Mirrors { get; set; } = new List<Mirror>();

        public static Playboard GetInstance()
        {
            return _instance ?? (_instance = new Playboard());
        }

        private Playboard()
        {
        }

        public void Paint(int posX, int posY, int height, SpriteBatch spriteBatch, ContentManager content)
        {
            int startPoint = posX;
            ClearBoard();
            LaserStart.ActivateLaser(LaserStart);
            for (int y = 0; y < Positions.GetLength(0); y++)
            {
                posX = 0 + startPoint;
                for (int x = 0; x < Positions.GetLength(1); x++)
                {

                    Figure fig = Positions[y, x];
                    fig.Paint(posX, posY, height, spriteBatch, content);
                    posX += height;
                }
                posY += height;
            }

        }

        public List<Neighbor> GetNeighbors(Figure fig)
        {
            List<Neighbor> neighbors = new List<Neighbor>();
            int x = fig.Pos.X;
            int y = fig.Pos.Y;
            //Pos Left
            if (x > 0)
            {
                Figure leftFigure = Positions[y, x - 1];
                neighbors.Add(new Neighbor() { Direction = NeighborDirection.Left, Fig = leftFigure });
            }
            //Pos Right
            if (x + 1 < Positions.GetLength(1))
            {
                Figure rightFigure = Positions[y, x + 1];
                neighbors.Add(new Neighbor() { Direction = NeighborDirection.Right, Fig = rightFigure });
            }
            //Pos Up
            if (y > 0)
            {
                Figure upFigure = Positions[y - 1, x];
                neighbors.Add(new Neighbor() { Direction = NeighborDirection.Up, Fig = upFigure });
            }
            //Pos Down
            if (y + 1 < Positions.GetLength(0))
            {
                Figure downFigure = Positions[y + 1, x];
                neighbors.Add(new Neighbor() { Direction = NeighborDirection.Down, Fig = downFigure });
            }
            return neighbors;
        }

        public Mirror GetMirror(CBlock cBlock)
        {
            Mirror closestMirror = null;
            int abstand = 0;
            int curAbstand = 0;
            foreach (Mirror mirror in Mirrors)
            {
                if (closestMirror is null)
                {
                    closestMirror = mirror;
                    abstand = Math.Abs(mirror.Pos.X - cBlock.Pos.X) + Math.Abs(mirror.Pos.Y - cBlock.Pos.Y);
                }
                else
                {
                    curAbstand = Math.Abs(mirror.Pos.X - cBlock.Pos.X) + Math.Abs(mirror.Pos.Y - cBlock.Pos.Y);
                    if (curAbstand < abstand)
                    {
                        abstand = curAbstand;
                        closestMirror = mirror;
                    }
                    if (curAbstand == abstand)
                    {
                        if (mirror.Pos.X < closestMirror.Pos.X || mirror.Pos.Y < closestMirror.Pos.Y)
                        {
                            abstand = curAbstand;
                            closestMirror = mirror;
                        }
                    }
                }
            }
            return closestMirror;
            //for (int y = 1; y < Positions.GetLength(0); y++)
            //{
            //    for (int x = 1; x < y; x++)
            //    {
            //        if (cBlock.Pos.Y + y < Positions.GetLength(0) &&
            //            Positions[cBlock.Pos.Y + y, cBlock.Pos.X] is Mirror &&
            //            !((Mirror)Positions[cBlock.Pos.Y + y, cBlock.Pos.X]).IsNotMovable)
            //        {
            //            return (Mirror)Positions[cBlock.Pos.Y + y, cBlock.Pos.X];
            //        }
            //        if (cBlock.Pos.Y + y < Positions.GetLength(0) && cBlock.Pos.X + x > 0 &&
            //            Positions[cBlock.Pos.Y + y, cBlock.Pos.X + x] is Mirror &&
            //            !((Mirror)Positions[cBlock.Pos.Y + y, cBlock.Pos.X + x]).IsNotMovable)
            //        {
            //            return (Mirror)Positions[cBlock.Pos.Y + y, cBlock.Pos.X + x];
            //        }
            //        if (cBlock.Pos.X + x > 0 && Positions[cBlock.Pos.Y, cBlock.Pos.X + x] is Mirror &&
            //            !((Mirror)Positions[cBlock.Pos.Y, cBlock.Pos.X + x]).IsNotMovable)
            //        {
            //            return (Mirror)Positions[cBlock.Pos.Y, cBlock.Pos.X + x];
            //        }
            //        if (cBlock.Pos.Y - y > 0 && cBlock.Pos.X + x > 0 &&
            //            Positions[cBlock.Pos.Y - y, cBlock.Pos.X + x] is Mirror &&
            //            !((Mirror)Positions[cBlock.Pos.Y - y, cBlock.Pos.X + x]).IsNotMovable)
            //        {
            //            return (Mirror)Positions[cBlock.Pos.Y - y, cBlock.Pos.X + x];
            //        }
            //        if (cBlock.Pos.Y - y > 0 && Positions[cBlock.Pos.Y - y, cBlock.Pos.X] is Mirror &&
            //            !((Mirror)Positions[cBlock.Pos.Y - y, cBlock.Pos.X]).IsNotMovable)
            //        {
            //            return (Mirror)Positions[cBlock.Pos.Y - y, cBlock.Pos.X];
            //        }
            //        if (cBlock.Pos.Y - y > 0 && cBlock.Pos.X - x < Positions.GetLength(1) &&
            //            Positions[cBlock.Pos.Y - y, cBlock.Pos.X - x] is Mirror &&
            //            !((Mirror)Positions[cBlock.Pos.Y - y, cBlock.Pos.X - x]).IsNotMovable)
            //        {
            //            return (Mirror)Positions[cBlock.Pos.Y - y, cBlock.Pos.X - x];
            //        }
            //        if (cBlock.Pos.X - x < Positions.GetLength(1) &&
            //            Positions[cBlock.Pos.Y, cBlock.Pos.X - x] is Mirror &&
            //            !((Mirror)Positions[cBlock.Pos.Y, cBlock.Pos.X - x]).IsNotMovable)
            //        {
            //            return (Mirror)Positions[cBlock.Pos.Y, cBlock.Pos.X - x];
            //        }
            //        if (cBlock.Pos.Y + y < Positions.GetLength(0) && cBlock.Pos.X - x < Positions.GetLength(1) &&
            //            Positions[cBlock.Pos.Y + y, cBlock.Pos.X - x] is Mirror &&
            //            !((Mirror)Positions[cBlock.Pos.Y + y, cBlock.Pos.X - x]).IsNotMovable)
            //        {
            //            return (Mirror)Positions[cBlock.Pos.Y + y, cBlock.Pos.X - x];
            //        }
            //}
            //}
            //return null;
        }
        public bool CanMove(Figure curFigure, Keys pressedKey)
        {
            Neighbor neighbor = null;
            switch (pressedKey)
            {
                case Keys.Down:
                    neighbor = curFigure.GetNeighbor(NeighborDirection.Down);
                    break;
                case Keys.Up:
                    neighbor = curFigure.GetNeighbor(NeighborDirection.Up);
                    break;
                case Keys.Left:
                    neighbor = curFigure.GetNeighbor(NeighborDirection.Left);
                    break;
                case Keys.Right:
                    neighbor = curFigure.GetNeighbor(NeighborDirection.Right);
                    break;
            }
            if (neighbor == null)
            {
                return false;
            }
            Figure figure2 = neighbor.Fig;
            if (!figure2.CanMove || !curFigure.CanMove || figure2 is Mirror)
            {
                return false;
            }
            return true;
        }
        public bool Move(Figure curFigure, Neighbor neighbor)
        {
            if (neighbor == null)
            {
                return false;
            }
            Figure figure2 = neighbor.Fig;
            if (!figure2.CanMove || !curFigure.CanMove || figure2 is Mirror || figure2 is MovableLR || figure2 is MovableUD || figure2 is Movable)
            {
                return false;
            }
            Figure temp;
            switch (neighbor.Direction)
            {

                case NeighborDirection.Up:
                    temp = Positions[curFigure.Pos.Y, curFigure.Pos.X];
                    Positions[curFigure.Pos.Y, curFigure.Pos.X] = Positions[curFigure.Pos.Y - 1, curFigure.Pos.X];
                    Positions[curFigure.Pos.Y - 1, curFigure.Pos.X] = temp;
                    Positions[curFigure.Pos.Y, curFigure.Pos.X].Pos = new Position() { X = curFigure.Pos.X, Y = curFigure.Pos.Y };
                    Positions[curFigure.Pos.Y - 1, curFigure.Pos.X].Pos = new Position() { X = curFigure.Pos.X, Y = curFigure.Pos.Y - 1 };
                    break;
                case NeighborDirection.Right:
                    temp = Positions[curFigure.Pos.Y, curFigure.Pos.X];
                    Positions[curFigure.Pos.Y, curFigure.Pos.X] = Positions[curFigure.Pos.Y, curFigure.Pos.X + 1];
                    Positions[curFigure.Pos.Y, curFigure.Pos.X + 1] = temp;
                    Positions[curFigure.Pos.Y, curFigure.Pos.X].Pos = new Position() { X = curFigure.Pos.X, Y = curFigure.Pos.Y };
                    Positions[curFigure.Pos.Y, curFigure.Pos.X + 1].Pos = new Position() { X = curFigure.Pos.X + 1, Y = curFigure.Pos.Y };
                    break;
                case NeighborDirection.Down:
                    temp = Positions[curFigure.Pos.Y, curFigure.Pos.X];
                    Positions[curFigure.Pos.Y, curFigure.Pos.X] = Positions[curFigure.Pos.Y + 1, curFigure.Pos.X];
                    Positions[curFigure.Pos.Y + 1, curFigure.Pos.X] = temp;
                    Positions[curFigure.Pos.Y, curFigure.Pos.X].Pos = new Position() { X = curFigure.Pos.X, Y = curFigure.Pos.Y };
                    Positions[curFigure.Pos.Y + 1, curFigure.Pos.X].Pos = new Position() { X = curFigure.Pos.X, Y = curFigure.Pos.Y + 1 };
                    break;
                case NeighborDirection.Left:
                    temp = Positions[curFigure.Pos.Y, curFigure.Pos.X];
                    Positions[curFigure.Pos.Y, curFigure.Pos.X] = Positions[curFigure.Pos.Y, curFigure.Pos.X - 1];
                    Positions[curFigure.Pos.Y, curFigure.Pos.X - 1] = temp;
                    Positions[curFigure.Pos.Y, curFigure.Pos.X].Pos = new Position() { X = curFigure.Pos.X, Y = curFigure.Pos.Y };
                    Positions[curFigure.Pos.Y, curFigure.Pos.X - 1].Pos = new Position() { X = curFigure.Pos.X - 1, Y = curFigure.Pos.Y };
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return true;
        }
        public async void InitBoardAsync(string level)
        {
            //var task = Serialize.SerializeLevel(Positions, level);
            //task.Wait();
            State = GameState.Loading;
            try
            {
                Positions = await LoadLevel.LevelAsync(level);
            }
            catch (Exception e)
            {
                var client = new HttpClient()
                {
                    BaseAddress = new Uri("http://laserballservice.azurewebsites.net/")
                };
                for (int i = 1; i < 14; i++)
                {
                    var response = await client.GetAsync("api/level?level=" + i);
                    var result = await response.Content.ReadAsStringAsync();
                    await Serialize.SerializeLevelAsync(result, "Level" + i);
                }
                Positions = await LoadLevel.LevelAsync(level);
            }
            foreach (Figure fig in Positions)
            {
                if (fig is Mirror)
                {
                    Mirrors.Add((Mirror)fig);
                }
            }
            State = GameState.Playing;
            LevelLoadedEvent args = new LevelLoadedEvent();
            OnLevelLoadedEvent(args);
            LaserStart = GetStartLaser();
        }
        protected virtual void OnLevelLoadedEvent(LevelLoadedEvent e)
        {
            LevelLoadedEvent?.Invoke(this, e);
        }

        private Laser GetStartLaser()
        {
            for (int y = 0; y < Positions.GetLength(0); y++)
            {
                for (int x = 0; x < Positions.GetLength(1); x++)
                {
                    Figure fig = Positions[y, x];
                    if (fig is Laser)
                    {
                        return (Laser)fig;
                    }
                }
            }
            throw new Exception("No Laser found");
        }


        public void ClearBoard()
        {
            for (int y = 0; y < Positions.GetLength(0); y++)
            {
                for (int x = 0; x < Positions.GetLength(1); x++)
                {
                    Figure fig = Positions[y, x];
                    fig.Status = FigureStatus.Inactive;
                    fig.Input = NeighborDirection.Undefined;
                    fig.Input2 = NeighborDirection.Undefined;
                    fig.Output2 = NeighborDirection.Undefined;
                    if (fig is CBlock)
                    {
                        ((CBlock)fig).Mirror = null;
                    }
                }
            }
        }
    }
}
