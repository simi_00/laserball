﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Laserball.UWP.Figures
{
    public class Point:EmptyFigure
    {
        public bool Collected { get; set; } = false;
        public Point(Position pos) : base(pos)
        {
            CanMove = false;
        }

        public override void ActivateLaser(Figure figure)
        {
            Collected = true;
            CanMove = true;
            base.ActivateLaser(figure);
        }

        public override void Paint(int posX, int posY, int height, SpriteBatch spriteBatch, ContentManager content)
        {
            if (Collected)
            {
                base.Paint(posX, posY, height, spriteBatch, content);
            }
            else
            {
                Texture = content.Load<Texture2D>("point");
                spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
            }
            
        }
    }
}
