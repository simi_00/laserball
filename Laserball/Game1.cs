﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Laserball.Events;
using Laserball.Figures;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PCLStorage;
using Point = Laserball.Figures.Point;

namespace Laserball
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        private Door _door;
        private MovableLR _movableLR;
        private MovableUD _movableUD;
        private Movable _movable;
        private int _points = 0;
        private bool _openDoor = false;
        private MouseState _mouseState;
        private KeyboardState _keyboard;
        private bool _allowInput = true;
        private int _selectedLevel = 33;
        private int i = 1;
        private Figures.Point _point;
        private bool _collectPoint = false;
        private const float Delay = 2; // seconds
        private float _remainingPointDelay = Delay;
        private float _remainingMoveLRDelay = Delay;
        private float _remainingMoveUDDelay = Delay;
        private float _remainingMoveDelay = Delay;
        private Keys _pressedKey;
        private bool _isScrollable;
        private int _posX = 0;
        private int _posY = 0;
        private int _height = 0;
        private int _diff;
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private Playboard _board = Playboard.GetInstance();
        private Figure _curFigure;
        private Texture2D _border;
        private MouseState _oldMouseState;
        private KeyboardState _oldKeyboardState;
        private float _remainingDoorDelay = Delay;
        private float _remainingCBlockDelay = Delay;
        private CBlock _destroyingCBlock;
        private SpriteFont _font;
        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            Window.AllowUserResizing = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            IsMouseVisible = false;
            _board.LevelLoadedEvent += (sender, args) =>
            {
                InitEvents();
            };
            _board.InitBoardAsync("Level" + _selectedLevel);
            base.Initialize();
        }

        private void InitEvents()
        {
            for (int y = 0; y < _board.Positions.GetLength(0); y++)
            {
                for (int x = 0; x < _board.Positions.GetLength(1); x++)
                {

                    Figure fig = _board.Positions[y, x];
                    switch (fig)
                    {
                        case Goal _:
                            Goal goal = (Goal)fig;
                            goal.GoalEvent += goal_GoalEvent;
                            break;
                        case Point _:
                            Figures.Point point = (Figures.Point)fig;
                            point.PointEvent += point_PointEvent;
                            break;
                        case Door _:
                            Door door = (Door)fig;
                            door.DoorEvent += door_DoorEvent;
                            break;
                        case CBlock _:
                            CBlock cBlock = (CBlock)fig;
                            cBlock.CBlockEvent += cBlock_CBlockEvent;
                            break;
                        case Bomb _:
                            Bomb bomb = (Bomb)fig;
                            bomb.BombEvent += bomb_BombEvent;
                            break;
                        case MovableLR _:
                            MovableLR movableLR = (MovableLR) fig;
                            movableLR.MoveLREvent += movableLR_MoveEvent;
                            break;
                        case MovableUD _:
                            MovableUD movableUD = (MovableUD)fig;
                            movableUD.MoveUDEvent += movableUD_MoveEvent;
                            break;
                        case Movable _:
                            Movable movable = (Movable)fig;
                            movable.MoveEvent += movable_MoveEvent;
                            break;
                    }
                }
            }
        }
        private void movable_MoveEvent(object sender, MoveEvent e)
        {
            _movable = (Movable)sender;
            if (_remainingMoveDelay <= 0)
            {
                Neighbor neighbor;
                switch (e.MoveDirection)
                {
                    case NeighborDirection.Up:
                        neighbor = _movable.GetNeighbor(NeighborDirection.Up);
                        if (neighbor.Fig.CanMove)
                        {
                            _board.Move(_movable, neighbor);
                        }
                        break;
                    case NeighborDirection.Down:
                        neighbor = _movable.GetNeighbor(NeighborDirection.Down);
                        if (neighbor.Fig.CanMove)
                        {
                            _board.Move(_movable, neighbor);
                        }
                        break;
                    case NeighborDirection.Left:
                        neighbor = _movable.GetNeighbor(NeighborDirection.Left);
                        if (neighbor.Fig.CanMove)
                        {
                            _board.Move(_movable, neighbor);
                        }
                        break;
                    case NeighborDirection.Right:
                        neighbor = _movable.GetNeighbor(NeighborDirection.Right);
                        if (neighbor.Fig.CanMove)
                        {
                            _board.Move(_movable, neighbor);
                        }
                        break;
                }
                switch (e.MoveDirection2)
                {
                    case NeighborDirection.Up:
                        neighbor = _movable.GetNeighbor(NeighborDirection.Up);
                        if (neighbor.Fig.CanMove)
                        {
                            _board.Move(_movable, neighbor);
                        }
                        break;
                    case NeighborDirection.Down:
                        neighbor = _movable.GetNeighbor(NeighborDirection.Down);
                        if (neighbor.Fig.CanMove)
                        {
                            _board.Move(_movable, neighbor);
                        }
                        break;
                    case NeighborDirection.Left:
                        neighbor = _movable.GetNeighbor(NeighborDirection.Left);
                        if (neighbor.Fig.CanMove)
                        {
                            _board.Move(_movable, neighbor);
                        }
                        break;
                    case NeighborDirection.Right:
                        neighbor = _movable.GetNeighbor(NeighborDirection.Right);
                        if (neighbor.Fig.CanMove)
                        {
                            _board.Move(_movable, neighbor);
                        }
                        break;
                }
                _movable = null;
                _remainingMoveDelay = Delay;
            }
        }
        private void movableUD_MoveEvent(object sender, MoveUDEvent e)
        {
            _movableUD = (MovableUD)sender;
            if (_remainingMoveUDDelay <= 0)
            {
                Neighbor neighbor;
                if (e.MoveDirection == NeighborDirection.Up)
                {
                    neighbor = _movableUD.GetNeighbor(NeighborDirection.Up);
                    if (neighbor.Fig.CanMove)
                    {
                        _board.Move(_movableUD, neighbor);
                    }
                }
                if (e.MoveDirection == NeighborDirection.Down)
                {
                    neighbor = _movableUD.GetNeighbor(NeighborDirection.Down);
                    if (neighbor.Fig.CanMove)
                    {
                        _board.Move(_movableUD, neighbor);
                    }
                }
                if (e.MoveDirection2 == NeighborDirection.Up)
                {
                    neighbor = _movableUD.GetNeighbor(NeighborDirection.Up);
                    if (neighbor.Fig.CanMove)
                    {
                        _board.Move(_movableUD, neighbor);
                    }
                }
                if (e.MoveDirection2 == NeighborDirection.Down)
                {
                    neighbor = _movableUD.GetNeighbor(NeighborDirection.Down);
                    if (neighbor.Fig.CanMove)
                    {
                        _board.Move(_movableUD, neighbor);
                    }
                }
                _movableUD = null;
                _remainingMoveUDDelay = Delay;
            }
        }
        private void movableLR_MoveEvent(object sender, MoveLREvent e)
        {
            _movableLR = (MovableLR) sender;
            if (_remainingMoveLRDelay <= 0)
            {
                Neighbor neighbor;
                if (e.MoveDirection == NeighborDirection.Right)
                {
                    neighbor = _movableLR.GetNeighbor(NeighborDirection.Right);
                    if (neighbor.Fig.CanMove)
                    {
                        _board.Move(_movableLR, neighbor);
                    }
                }
                if (e.MoveDirection == NeighborDirection.Left)
                {
                    neighbor = _movableLR.GetNeighbor(NeighborDirection.Left);
                    if (neighbor.Fig.CanMove)
                    {
                        _board.Move(_movableLR, neighbor);
                    }
                }
                if (e.MoveDirection2 == NeighborDirection.Right)
                {
                    neighbor = _movableLR.GetNeighbor(NeighborDirection.Right);
                    if (neighbor.Fig.CanMove)
                    {
                        _board.Move(_movableLR, neighbor);
                    }
                }
                if (e.MoveDirection2 == NeighborDirection.Left)
                {
                    neighbor = _movableLR.GetNeighbor(NeighborDirection.Left);
                    if (neighbor.Fig.CanMove)
                    {
                        _board.Move(_movableLR, neighbor);
                    }
                }
                _movableLR = null;
                _remainingMoveLRDelay = Delay;
            }
        }

        private void bomb_BombEvent(object sender, BombEvent e)
        {
            _board.State = GameState.GameOver;
        }

        private void cBlock_CBlockEvent(object sender, CBlockEvent e)
        {
            _destroyingCBlock = (CBlock)sender;
            if (_remainingCBlockDelay <= 0 && _destroyingCBlock.CBlockStatus < 5)
            {
                _destroyingCBlock.CBlockStatus++;
                _remainingCBlockDelay = Delay;
            }
        }

        private void door_DoorEvent(object sender, DoorEvent e)
        {
            _door = (Door)sender;
            _openDoor = true;
            if (_remainingDoorDelay <= 0)
            {
                _door.DoorPosition = DoorStatus.Open;
                _remainingDoorDelay = Delay;
                _openDoor = false;
            }
        }

        private void point_PointEvent(object sender, PointEvent e)
        {
            if (_point != null && !_point.Equals(sender))
            {
                _point.Collected = true;
                _point.CanMove = true;
                _remainingPointDelay = Delay;
                _collectPoint = false;
                _points += 100;
                _point = null;
            }
            _point = (Figures.Point)sender;
            _collectPoint = true;
            if (_board.IsMoving && _board.CanMove(_curFigure, _pressedKey))
            {
                ((Figures.Point)sender).Collected = true;
                ((Figures.Point)sender).CanMove = true;
                _points += 100;
                _point = null;
            }
            if (_point != null && _remainingPointDelay <= 0)
            {
                _point.Collected = true;
                _point.CanMove = true;
                _remainingPointDelay = Delay;
                _collectPoint = false;
                _points += 100;
                _point = null;
            }
        }
        private void FinalEvents()
        {
            for (int y = 0; y < _board.Positions.GetLength(0); y++)
            {
                for (int x = 0; x < _board.Positions.GetLength(1); x++)
                {

                    Figure fig = _board.Positions[y, x];
                    switch (fig)
                    {
                        case Goal _:
                            Goal goal = (Goal)fig;
                            goal.GoalEvent -= goal_GoalEvent;
                            break;
                        case Point _:
                            Figures.Point point = (Figures.Point)fig;
                            point.PointEvent -= point_PointEvent;
                            break;
                        case Door _:
                            Door door = (Door)fig;
                            door.DoorEvent -= door_DoorEvent;
                            break;
                        case CBlock _:
                            CBlock cBlock = (CBlock)fig;
                            cBlock.CBlockEvent -= cBlock_CBlockEvent;
                            break;
                        case Bomb _:
                            Bomb bomb = (Bomb)fig;
                            bomb.BombEvent -= bomb_BombEvent;
                            break;
                        case MovableLR _:
                            MovableLR movableLR = (MovableLR)fig;
                            movableLR.MoveLREvent -= movableLR_MoveEvent;
                            break;
                        case MovableUD _:
                            MovableUD movableUD = (MovableUD)fig;
                            movableUD.MoveUDEvent -= movableUD_MoveEvent;
                            break;
                        case Movable _:
                            Movable movable = (Movable)fig;
                            movable.MoveEvent -= movable_MoveEvent;
                            break;
                    }
                }
            }
        }
        private void goal_GoalEvent(object sender, GoalEvent e)
        {
            if (i < 8)
            {
                _allowInput = false;
                ((Goal)sender).Texture = Content.Load<Texture2D>("goal-" + i);
                i++;
                Task.Delay(100).Wait();
                return;
            }
            _selectedLevel++;
            if (_selectedLevel <= 13)
            {
                FinalEvents();
                _board.InitBoardAsync("Level" + _selectedLevel);
                i = 1;
                InitEvents();
                _allowInput = true;
            }
            else
            {
                _allowInput = false;
            }
            _board.Mirrors.Clear();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            _font = Content.Load<SpriteFont>("arial");
            _border = Content.Load<Texture2D>("border");
            SoundEffect song = Content.Load<SoundEffect>("lb");
            SoundEffectInstance soundEffectInstance = song.CreateInstance();
            soundEffectInstance.IsLooped = true;
            soundEffectInstance.Play();
            // TODO: use this.Content to load your game content here

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (_board.State == GameState.Playing)
            {
                if (_allowInput)
                {
                    _mouseState = Mouse.GetState();
                    _keyboard = Keyboard.GetState();
                }
                if (_collectPoint)
                {
                    var timer = (float)gameTime.ElapsedGameTime.TotalSeconds;
                    _remainingPointDelay -= timer;
                }
                else
                {
                    _remainingPointDelay = Delay;
                }
                if (_point == null)
                {
                    _remainingPointDelay = Delay;
                }
                if (_point?.Status == FigureStatus.Inactive && !_point.Collected)
                {
                    _point.Collected = true;
                    _point.CanMove = true;
                    _remainingPointDelay = Delay;
                    _points += 100;
                    _point = null;
                }
                if (_destroyingCBlock != null)
                {
                    var timer = (float)gameTime.ElapsedGameTime.TotalSeconds;
                    _remainingCBlockDelay -= timer;
                }
                if (_destroyingCBlock?.Status == FigureStatus.Inactive)
                {
                    _remainingCBlockDelay = Delay;
                    _destroyingCBlock = null;
                }
                if (_movableLR != null)
                {
                    var timer = (float) gameTime.ElapsedGameTime.TotalSeconds;
                    _remainingMoveLRDelay -= timer;
                }
                if (_movableUD != null)
                {
                    var timer = (float)gameTime.ElapsedGameTime.TotalSeconds;
                    _remainingMoveUDDelay -= timer;
                }
                if (_movable != null)
                {
                    var timer = (float)gameTime.ElapsedGameTime.TotalSeconds;
                    _remainingMoveDelay -= timer;
                }
                if (_openDoor)
                {
                    var timer = (float)gameTime.ElapsedGameTime.TotalSeconds;
                    _remainingDoorDelay -= timer;
                }
                if (_door?.DoorPosition == DoorStatus.Open || (_door?.DoorPosition == DoorStatus.Closed && _door?.Status == FigureStatus.Inactive))
                {
                    _remainingDoorDelay = Delay;
                }
                _height = GraphicsDevice.PresentationParameters.Bounds.Height / _board.Positions.GetLength(0);
                if (_height * _board.Positions.GetLength(1) > GraphicsDevice.PresentationParameters.Bounds.Width)
                {
                    _isScrollable = true;
                }
                if (_isScrollable && _mouseState.X > GraphicsDevice.PresentationParameters.Bounds.Width * 0.8 && _posX * -1 < _height * _board.Positions.GetLength(1) - GraphicsDevice.PresentationParameters.Bounds.Width)
                {
                    _posX -= 10;
                }
                if (_isScrollable && _mouseState.X < GraphicsDevice.PresentationParameters.Bounds.Width * 0.2 && _posX < 0)
                {
                    _posX += 10;
                }
                if (_mouseState.Y / _height < _board.Positions.GetLength(0) && ((_mouseState.X - _diff) / _height) + ((_posX * -1) / _height) + 1 < _board.Positions.GetLength(1) && _mouseState.X >= 0 && _mouseState.Y >= 0)
                {
                    _curFigure = _posX < 0 ? _board.Positions[_mouseState.Y / _height, ((_mouseState.X - _diff) / _height) + ((_posX * -1) / _height) + 1] : _board.Positions[_mouseState.Y / _height, (_mouseState.X) / _height];
                }
                if (_mouseState.LeftButton == ButtonState.Pressed && _curFigure is Mirror && _oldMouseState.LeftButton == ButtonState.Released && _curFigure.IsNotMovable == false && (_mouseState.Y / _height < _board.Positions.GetLength(0) && ((_mouseState.X - _diff) / _height) + ((_posX * -1) / _height) + 1 < _board.Positions.GetLength(1) && _mouseState.X >= 0 && _mouseState.Y >= 0))
                {
                    if (_posX < 0)
                    {
                        ((Mirror)_board.Positions[_mouseState.Y / _height, ((_mouseState.X - _diff) / _height) + ((_posX * -1) / _height) + 1]).Direction = ((Mirror)_curFigure).Direction == MirrorDirection.Forward ? MirrorDirection.Backward : MirrorDirection.Forward;
                    }
                    else
                    {
                        ((Mirror)_board.Positions[_mouseState.Y / _height, (_mouseState.X / _height)]).Direction = ((Mirror)_curFigure).Direction == MirrorDirection.Forward ? MirrorDirection.Backward : MirrorDirection.Forward;
                    }
                }
                if (_curFigure is Mirror && _curFigure.IsNotMovable == false)
                {
                    if ((_keyboard.IsKeyDown(Keys.Down) && !_oldKeyboardState.IsKeyDown(Keys.Down)) || _pressedKey == Keys.Down)
                    {
                        _pressedKey = Keys.Down;
                        if (_board.Move(_curFigure, _curFigure.GetNeighbor(NeighborDirection.Down)))
                        {
                            _allowInput = false;
                            _board.IsMoving = true;
                            Mouse.SetPosition(_mouseState.X, _mouseState.Y + _height);
                            _mouseState = Mouse.GetState();
                            if (_mouseState.Y / _height < _board.Positions.GetLength(0) && ((_mouseState.X - _diff) / _height) + ((_posX * -1) / _height) + 1 < _board.Positions.GetLength(1))
                            {
                                _curFigure = _posX < 0 ? _board.Positions[_mouseState.Y / _height, ((_mouseState.X - _diff) / _height) + ((_posX * -1) / _height) + 1] : _board.Positions[_mouseState.Y / _height, (_mouseState.X) / _height];
                            }
                        }
                        else
                        {
                            _allowInput = true;
                            _pressedKey = Keys.None;
                            _board.IsMoving = false;
                        }
                    }
                    if ((_keyboard.IsKeyDown(Keys.Up) && !_oldKeyboardState.IsKeyDown(Keys.Up)) || _pressedKey == Keys.Up)
                    {
                        _pressedKey = Keys.Up;
                        if (_board.Move(_curFigure, _curFigure.GetNeighbor(NeighborDirection.Up)))
                        {
                            _allowInput = false;
                            _board.IsMoving = true;
                            Mouse.SetPosition(_mouseState.X, _mouseState.Y - _height);
                            _mouseState = Mouse.GetState();
                            if (_mouseState.Y / _height < _board.Positions.GetLength(0) && ((_mouseState.X - _diff) / _height) + ((_posX * -1) / _height) + 1 < _board.Positions.GetLength(1))
                            {
                                _curFigure = _posX < 0 ? _board.Positions[_mouseState.Y / _height, ((_mouseState.X - _diff) / _height) + ((_posX * -1) / _height) + 1] : _board.Positions[_mouseState.Y / _height, (_mouseState.X) / _height];
                            }
                        }
                        else
                        {
                            _allowInput = true;
                            _pressedKey = Keys.None;
                            _board.IsMoving = false;
                        }
                    }
                    if ((_keyboard.IsKeyDown(Keys.Left) && !_oldKeyboardState.IsKeyDown(Keys.Left)) || _pressedKey == Keys.Left)
                    {
                        _pressedKey = Keys.Left;
                        if (_board.Move(_curFigure, _curFigure.GetNeighbor(NeighborDirection.Left)))
                        {
                            _allowInput = false;
                            _board.IsMoving = true;
                            Mouse.SetPosition(_mouseState.X - _height, _mouseState.Y);
                            _mouseState = Mouse.GetState();
                            if (_mouseState.Y / _height < _board.Positions.GetLength(0) && ((_mouseState.X - _diff) / _height) + ((_posX * -1) / _height) + 1 < _board.Positions.GetLength(1))
                            {
                                _curFigure = _posX < 0 ? _board.Positions[_mouseState.Y / _height, ((_mouseState.X - _diff) / _height) + ((_posX * -1) / _height) + 1] : _board.Positions[_mouseState.Y / _height, (_mouseState.X) / _height];
                            }
                        }
                        else
                        {
                            _allowInput = true;
                            _pressedKey = Keys.None;
                            _board.IsMoving = false;
                        }
                    }
                    if ((_keyboard.IsKeyDown(Keys.Right) && !_oldKeyboardState.IsKeyDown(Keys.Right)) ||
                        _pressedKey == Keys.Right)
                    {
                        _pressedKey = Keys.Right;
                        if (_board.Move(_curFigure, _curFigure.GetNeighbor(NeighborDirection.Right)))
                        {
                            _allowInput = false;
                            _board.IsMoving = true;
                            Mouse.SetPosition(_mouseState.X + _height, _mouseState.Y);
                            _mouseState = Mouse.GetState();
                            if (_mouseState.Y / _height < _board.Positions.GetLength(0) &&
                                ((_mouseState.X - _diff) / _height) + ((_posX * -1) / _height) + 1 < _board.Positions.GetLength(1))
                            {
                                _curFigure = _posX < 0
                                    ? _board.Positions[
                                        _mouseState.Y / _height, ((_mouseState.X - _diff) / _height) + ((_posX * -1) / _height) + 1
                                    ]
                                    : _board.Positions[_mouseState.Y / _height, (_mouseState.X) / _height];
                            }
                        }
                        else
                        {
                            _allowInput = true;
                            _pressedKey = Keys.None;
                            _board.IsMoving = false;
                        }
                    }
                }
                if (_keyboard.IsKeyDown(Keys.A) && !_oldKeyboardState.IsKeyDown(Keys.A))
                {
                }
                _diff = (((_posX * -1) - ((_posX * -1) / _height) * _height));
                _oldMouseState = _mouseState;
                _oldKeyboardState = _keyboard;
            }
            base.Update(gameTime);
        }
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            MouseState mouseState = Mouse.GetState();
            GraphicsDevice.Clear(Color.Black);
            _spriteBatch.Begin();
            if (_board.State == GameState.Playing)
            {
                Window.Title = "Laserball - Points: " + _points;
                _board.Paint(_posX, _posY, _height, _spriteBatch, Content);
                _spriteBatch.Draw(_border,
                    _posX < 0
                        ? new Rectangle(((((mouseState.X - _diff) / _height) + 1) * _height) - _diff,
                            (mouseState.Y / _height) * _height, _height, _height)
                        : new Rectangle(((((mouseState.X) / _height)) * _height), (mouseState.Y / _height) * _height, _height,
                            _height), Color.White);
                foreach (Figure fig in _board.Positions)
                {
                    if (fig is CBlock)
                    {
                        CBlock cBlock = (CBlock)fig;
                        if (cBlock.Mirror != null)
                        {
                            if (cBlock.CBlockStatus < 2)
                            {
                                _spriteBatch.Draw(Content.Load<Texture2D>("border-green"), new Rectangle(cBlock.Mirror.Pos.X * _height + _posX, cBlock.Mirror.Pos.Y * _height, _height, _height), Color.White);
                            }
                            if (cBlock.CBlockStatus >= 2 && cBlock.CBlockStatus < 4)
                            {
                                _spriteBatch.Draw(Content.Load<Texture2D>("border-orange"), new Rectangle(cBlock.Mirror.Pos.X * _height + _posX, cBlock.Mirror.Pos.Y * _height, _height, _height), Color.White);
                            }
                            if (cBlock.CBlockStatus >= 4)
                            {
                                _spriteBatch.Draw(Content.Load<Texture2D>("border-red"), new Rectangle(cBlock.Mirror.Pos.X * _height + _posX, cBlock.Mirror.Pos.Y * _height, _height, _height), Color.White);
                            }
                        }
                    }
                }
            }
            if (_board.State == GameState.Loading)
            {
                _spriteBatch.DrawString(_font,"Loading...",new Vector2(0,0),Color.White);
            }
            if (_board.State == GameState.GameOver)
            { 
                _spriteBatch.DrawString(_font,"Bombe getroffen - Game Over \nPoints: "+_points,new Vector2(0,0),Color.White);
            }
            _spriteBatch.End();
            base.Draw(gameTime);
        }
        //TODO
        //public int GetMousePositionX()
        //{
        //    return Windows.UI.Xaml.Window.Current.CoreWindow.PointerPosition.X - 
        //}
    }
}
