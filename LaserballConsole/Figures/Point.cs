﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserballConsole.Figures
{
    public class Point:Figure
    {
        public bool IsCollected { get; set; } = false;
        public Point(Position pos) : base(pos)
        {
        }

        public override void ActivateLaser(Figure figure)
        {
            IsCollected = true;
        }

        public override void Paint()
        {
            Console.Write("P");
            //TEST
        }
    }
}
