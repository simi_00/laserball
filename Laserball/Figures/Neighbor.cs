﻿namespace Laserball.Figures
{
    public enum NeighborDirection
    {
       Undefined, Up, Right,Down,Left
    }
    public class Neighbor
    {
        public Figure Fig { get; set; }
        public NeighborDirection Direction { get; set; }
    }
}