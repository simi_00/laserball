﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Laserball.Figures
{
    public enum BoderOrientation
    {
        Vertical, Horizontal, Center, TL, TR, BL, BR, LeftEnd, RightEnd, BottomEnd, TopEnd, Round, Cross
    }
    public class Border : Figure
    {
        public BoderOrientation Orientation { get; set; } = BoderOrientation.Horizontal;
        public Border(Position pos) : base(pos)
        {
        }

        public override void ActivateLaser(Figure figure)
        {
            if (Input == NeighborDirection.Undefined)
            {
                Input = GetDirection(figure);
            }
            else
            {
                Input2 = GetDirection(figure);
            }
            Status = FigureStatus.Active;
        }

        public override void Paint(int posX, int posY, int height, SpriteBatch spriteBatch, ContentManager content)
        {
            switch (Orientation)
            {
                case BoderOrientation.Vertical:
                    Texture = content.Load<Texture2D>("border-ver");
                    spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
                    if (Status == FigureStatus.Active)
                    {
                        switch (Input)
                        {

                            case NeighborDirection.Right:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"),
                                    new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.7), 0)), posY,
                                        Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                break;
                            case NeighborDirection.Left:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                break;
                        }
                        if (Input2 != NeighborDirection.Undefined)
                        {
                            switch (Input2)
                            {

                                case NeighborDirection.Right:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-hor"),
                                        new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.7), 0)), posY,
                                            Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                    break;
                                case NeighborDirection.Left:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                    break;
                            }
                        }
                    }
                    break;
                case BoderOrientation.Horizontal:
                    Texture = content.Load<Texture2D>("border-hor");
                    spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
                    if (Status == FigureStatus.Active)
                    {
                        switch (Input)
                        {
                            case NeighborDirection.Up:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                break;
                            case NeighborDirection.Down:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.7), 0)), height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                break;
                        }
                        if (Input2 != NeighborDirection.Undefined)
                        {
                            switch (Input2)
                            {
                                case NeighborDirection.Up:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                    break;
                                case NeighborDirection.Down:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.7), 0)), height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                    break;
                            }
                        }
                    }
                    break;
                case BoderOrientation.Center:
                    Texture = content.Load<Texture2D>("border-center");
                    spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
                    if (Status == FigureStatus.Active)
                    {
                        switch (Input)
                        {
                            case NeighborDirection.Up:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.2, 0))), Color.White);
                                break;
                            case NeighborDirection.Left:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.2, 0)), height), Color.White);
                                break;
                            case NeighborDirection.Right:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"),
                                    new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.8), 0)), posY,
                                        Convert.ToInt32(Math.Round(height * 0.2, 0)), height), Color.White);
                                break;
                            case NeighborDirection.Down:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.8), 0)), height, Convert.ToInt32(Math.Round(height * 0.2, 0))), Color.White);
                                break;
                        }
                        if (Input2 != NeighborDirection.Undefined)
                        {
                            switch (Input2)
                            {
                                case NeighborDirection.Up:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.2, 0))), Color.White);
                                    break;
                                case NeighborDirection.Left:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.2, 0)), height), Color.White);
                                    break;
                                case NeighborDirection.Right:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-hor"),
                                        new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.8), 0)), posY,
                                            Convert.ToInt32(Math.Round(height * 0.2, 0)), height), Color.White);
                                    break;
                                case NeighborDirection.Down:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.8), 0)), height, Convert.ToInt32(Math.Round(height * 0.2, 0))), Color.White);
                                    break;
                            }
                        }
                    }
                    break;
                case BoderOrientation.TL:
                    Texture = content.Load<Texture2D>("border-tl");
                    spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
                    if (Status == FigureStatus.Active)
                    {
                        switch (Input)
                        {
                            case NeighborDirection.Up:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                break;
                            case NeighborDirection.Left:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                break;
                        }
                        if (Input2 != NeighborDirection.Undefined)
                        {
                            switch (Input2)
                            {
                                case NeighborDirection.Up:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                    break;
                                case NeighborDirection.Left:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                    break;
                            }
                        }
                    }
                    break;
                case BoderOrientation.TR:
                    Texture = content.Load<Texture2D>("border-tr");
                    spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
                    if (Status == FigureStatus.Active)
                    {
                        switch (Input)
                        {
                            case NeighborDirection.Up:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                break;
                            case NeighborDirection.Right:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"),
                                    new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.7), 0)), posY,
                                        Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                break;
                        }
                        if (Input2 != NeighborDirection.Undefined)
                        {
                            switch (Input2)
                            {
                                case NeighborDirection.Up:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                    break;
                                case NeighborDirection.Right:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-hor"),
                                        new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.7), 0)), posY,
                                            Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                    break;
                            }
                        }
                    }
                    break;
                case BoderOrientation.BL:
                    Texture = content.Load<Texture2D>("border-bl");
                    spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
                    if (Status == FigureStatus.Active)
                    {
                        switch (Input)
                        {
                            case NeighborDirection.Left:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                break;
                            case NeighborDirection.Down:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.7), 0)), height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                break;
                        }
                        if (Input2 != NeighborDirection.Undefined)
                        {
                            switch (Input2)
                            {
                                case NeighborDirection.Left:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                    break;
                                case NeighborDirection.Down:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.7), 0)), height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                    break;
                            }
                        }
                    }
                    break;
                case BoderOrientation.BR:
                    Texture = content.Load<Texture2D>("border-br");
                    spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
                    if (Status == FigureStatus.Active)
                    {
                        switch (Input)
                        {
                            case NeighborDirection.Right:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"),
                                        new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.7), 0)), posY,
                                            Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                break;
                            case NeighborDirection.Down:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.7), 0)), height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                break;
                        }
                        if (Input2 != NeighborDirection.Undefined)
                        {
                            switch (Input2)
                            {
                                case NeighborDirection.Right:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-hor"),
                                        new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.7), 0)), posY,
                                            Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                    break;
                                case NeighborDirection.Down:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.7), 0)), height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                    break;
                            }
                        }
                    }
                    break;
                case BoderOrientation.Cross:
                    Texture = content.Load<Texture2D>("border-cross");
                    spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
                    break;
                case BoderOrientation.BottomEnd:
                    Texture = content.Load<Texture2D>("border-end-bot");
                    spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
                    if (Status == FigureStatus.Active)
                    {
                        switch (Input)
                        {
                            case NeighborDirection.Right:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"),
                                    new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.7), 0)), posY,
                                        Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                break;
                            case NeighborDirection.Down:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.7), 0)), height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                break;
                            case NeighborDirection.Left:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                break;

                        }
                        if (Input2 != NeighborDirection.Undefined)
                        {
                            switch (Input2)
                            {
                                case NeighborDirection.Right:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-hor"),
                                        new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.7), 0)), posY,
                                            Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                    break;
                                case NeighborDirection.Down:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.7), 0)), height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                    break;
                                case NeighborDirection.Left:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                    break;
                            }
                        }
                    }
                    break;
                case BoderOrientation.TopEnd:
                    Texture = content.Load<Texture2D>("border-end-top");
                    spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
                    if (Status == FigureStatus.Active)
                    {
                        switch (Input)
                        {
                            case NeighborDirection.Right:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"),
                                    new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.7), 0)), posY,
                                        Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                break;
                            case NeighborDirection.Up:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                break;
                            case NeighborDirection.Left:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                break;

                        }
                        if (Input2 != NeighborDirection.Undefined)
                        {
                            switch (Input2)
                            {
                                case NeighborDirection.Right:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-hor"),
                                        new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.7), 0)), posY,
                                            Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                    break;
                                case NeighborDirection.Up:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                    break;
                                case NeighborDirection.Left:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                    break;
                            }
                        }
                    }
                    break;
                case BoderOrientation.LeftEnd:
                    Texture = content.Load<Texture2D>("border-end-left");
                    spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
                    if (Status == FigureStatus.Active)
                    {
                        switch (Input)
                        {
                            case NeighborDirection.Down:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.7), 0)), height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                break;
                            case NeighborDirection.Up:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                break;
                            case NeighborDirection.Left:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                break;

                        }
                        if (Input2 != NeighborDirection.Undefined)
                        {
                            switch (Input2)
                            {
                                case NeighborDirection.Down:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.7), 0)), height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                    break;
                                case NeighborDirection.Up:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                    break;
                                case NeighborDirection.Left:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                    break;
                            }
                        }
                    }
                    break;
                case BoderOrientation.RightEnd:
                    Texture = content.Load<Texture2D>("border-end-right");
                    spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
                    if (Status == FigureStatus.Active)
                    {
                        switch (Input)
                        {
                            case NeighborDirection.Down:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.7), 0)), height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                break;
                            case NeighborDirection.Up:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                break;
                            case NeighborDirection.Right:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"),
                                        new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.7), 0)), posY,
                                            Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                break;

                        }
                        if (Input2 != NeighborDirection.Undefined)
                        {
                            switch (Input2)
                            {
                                case NeighborDirection.Down:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.7), 0)), height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                    break;
                                case NeighborDirection.Up:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.3, 0))), Color.White);
                                    break;
                                case NeighborDirection.Right:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-hor"),
                                        new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.7), 0)), posY,
                                            Convert.ToInt32(Math.Round(height * 0.3, 0)), height), Color.White);
                                    break;
                            }
                        }
                    }
                    break;
                    case BoderOrientation.Round:
                    Texture = content.Load<Texture2D>("border-round");
                    spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
                    if (Status == FigureStatus.Active)
                    {
                        switch (Input)
                        {
                            case NeighborDirection.Up:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.2, 0))), Color.White);
                                break;
                            case NeighborDirection.Left:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.2, 0)), height), Color.White);
                                break;
                            case NeighborDirection.Right:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-hor"),
                                    new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.8), 0)), posY,
                                        Convert.ToInt32(Math.Round(height * 0.2, 0)), height), Color.White);
                                break;
                            case NeighborDirection.Down:
                                spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.8), 0)), height, Convert.ToInt32(Math.Round(height * 0.2, 0))), Color.White);
                                break;
                        }
                        if (Input2 != NeighborDirection.Undefined)
                        {
                            switch (Input2)
                            {
                                case NeighborDirection.Up:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, posY, height, Convert.ToInt32(Math.Round(height * 0.2, 0))), Color.White);
                                    break;
                                case NeighborDirection.Left:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-hor"), new Rectangle(posX, posY, Convert.ToInt32(Math.Round(height * 0.2, 0)), height), Color.White);
                                    break;
                                case NeighborDirection.Right:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-hor"),
                                        new Rectangle(Convert.ToInt32(Math.Round(posX + (height * 0.8), 0)), posY,
                                            Convert.ToInt32(Math.Round(height * 0.2, 0)), height), Color.White);
                                    break;
                                case NeighborDirection.Down:
                                    spriteBatch.Draw(content.Load<Texture2D>("laser-ver"), new Rectangle(posX, Convert.ToInt32(Math.Round(posY + (height * 0.8), 0)), height, Convert.ToInt32(Math.Round(height * 0.2, 0))), Color.White);
                                    break;
                            }
                        }
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}