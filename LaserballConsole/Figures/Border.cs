﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LaserballConsole.Figures
{
    public class Border:Figure
    {
        public Border(Position pos) : base(pos)
        {
        }

        public override void ActivateLaser(Figure figure)
        {
            
        }

        public override void Paint()
        {
            Console.Write("B");
        }
    }
}