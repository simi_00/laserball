﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Laserball.UWP.Figures
{
    public enum BoderOrientation
    {
        Vertical, Horizontal, TL,TR,BL,BR
    }
    public class Border : Figure
    {
        public BoderOrientation Orientation { get; set; } = BoderOrientation.Horizontal;
        public Border(Position pos) : base(pos)
        {
        }

        public override void ActivateLaser(Figure figure)
        {

        }

        public override void Paint(int posX, int posY, int height, SpriteBatch spriteBatch, ContentManager content)
        {
            switch (Orientation)
            {
                case BoderOrientation.Vertical:
                    Texture = content.Load<Texture2D>("border-ver");
                    break;
                case BoderOrientation.Horizontal:
                    Texture = content.Load<Texture2D>("border-hor");
                    break;
                case BoderOrientation.TL:
                    Texture = content.Load<Texture2D>("border-tl");
                    break;
                case BoderOrientation.TR:
                    Texture = content.Load<Texture2D>("border-tr");
                    break;
                case BoderOrientation.BL:
                    Texture = content.Load<Texture2D>("border-bl");
                    break;
                case BoderOrientation.BR:
                    Texture = content.Load<Texture2D>("border-br");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
        }
    }
}