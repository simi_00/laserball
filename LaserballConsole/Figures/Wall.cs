﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserballConsole.Figures
{
    public class Wall : Figure
    {
        public Wall(Position pos) : base(pos)
        {
            
        }

        public override void ActivateLaser(Figure figure)
        {
            NeighborDirection input = GetDirection(figure);
            NeighborDirection output = GetOutputDirection(input);
            Neighbor nextNeighbor = GetNeighbor(output);
            if (nextNeighbor != null)
            {
                Figure nextFigure = nextNeighbor.Fig;
                nextFigure.ActivateLaser(this);
            }
            Status = FigureStatus.Active;
        }

        public override void Paint()
        {
            Console.Write("W");
        }
        public NeighborDirection GetOutputDirection(NeighborDirection inputDirection)
        {
            switch (inputDirection)
            {
                case NeighborDirection.Up:
                    return NeighborDirection.Down;
                case NeighborDirection.Right:
                    return NeighborDirection.Left;
                case NeighborDirection.Down:
                    return NeighborDirection.Up;
                case NeighborDirection.Left:
                    return NeighborDirection.Right;
                default:
                    throw new ArgumentOutOfRangeException(nameof(inputDirection), inputDirection, null);
            }
        }
    }
}
