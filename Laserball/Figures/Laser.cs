﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Laserball.Figures
{
    public class Laser:Figure
    {
        public Figure fig { get; set; }
        public Laser(Position pos) : base(pos)
        {
        }

        public override void ActivateLaser(Figure figure)
        {
            if (Status == FigureStatus.Inactive)
            {
                fig = figure;
                Neighbor nextNeighbor = GetNeighbor(NeighborDirection.Right);
                nextNeighbor.Fig.ActivateLaser(this);
            }
        }


        public override void Paint(int posX, int posY, int height, SpriteBatch spriteBatch, ContentManager content)
        {
            Texture = content.Load<Texture2D>("laser");

            spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
        }
    }
}
