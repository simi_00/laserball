﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Laserball.Figures;
using Newtonsoft.Json;

namespace LaserballLevelConverter
{
    public partial class Form1 : Form
    {
        private string json;
        private int lineNumber = 1;
        private string levelNumber;
        private int _x = 0;
        private int _y = 0;
        private string line = "";
        private Figure[,] _positions = new Figure[10, 10];
        MemoryStream stream = new MemoryStream();
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_open_Click(object sender, EventArgs e)
        {
            using (StreamWriter wrt = new StreamWriter(@"C:\Users\Simi\Desktop\LL\Level" + levelNumber))
            {
                wrt.Write(json);
            }
        }

        private void btn_convert_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                levelNumber = openFileDialog1.SafeFileName.Substring(3);
                using (StreamReader reader = new StreamReader(openFileDialog1.OpenFile()))
                {
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (lineNumber > 3)
                        {
                            try
                            {
                                switch (Convert.ToInt32(line))
                                {
                                    case 0:
                                        _positions[_y, _x] = new Air(new Position() { X = _x, Y = _y });
                                        break;
                                    case 1:
                                        _positions[_y, _x] = new EmptyFigure(new Position() { X = _x, Y = _y });
                                        break;
                                    case 2:
                                        _positions[_y, _x] = new Goal(new Position() { X = _x, Y = _y });
                                        break;
                                    case 4:
                                        _positions[_y, _x] = new Wall(new Position() { X = _x, Y = _y });
                                        break;
                                    case 5:
                                        _positions[_y, _x] = new CBlock(new Position() { X = _x, Y = _y }){CBlockMode = CBlockMode.Destroying};
                                        break;
                                    case 6:
                                        _positions[_y, _x] = new CBlock(new Position() { X = _x, Y = _y }) { CBlockStatus = 1, CBlockMode = CBlockMode.Destroying};
                                        break;
                                    case 7:
                                        _positions[_y, _x] = new CBlock(new Position() { X = _x, Y = _y }) { CBlockStatus = 2, CBlockMode = CBlockMode.Destroying };
                                        break;
                                    case 8:
                                        _positions[_y, _x] = new CBlock(new Position() { X = _x, Y = _y }) { CBlockStatus = 3, CBlockMode = CBlockMode.Destroying };
                                        break;
                                    case 9:
                                        _positions[_y, _x] = new CBlock(new Position() { X = _x, Y = _y }) { CBlockStatus = 4, CBlockMode = CBlockMode.Destroying };
                                        break;
                                    case 10:
                                        _positions[_y, _x] = new CBlock(new Position() { X = _x, Y = _y }) { CBlockStatus = 5, CBlockMode = CBlockMode.Destroying };
                                        break;
                                    case 11:
                                        _positions[_y, _x] = new Mirror(new Position() { X = _x, Y = _y });
                                        break;
                                    case 12:
                                        _positions[_y, _x] = new Mirror(new Position() { X = _x, Y = _y }) { Direction = MirrorDirection.Backward };
                                        break;
                                    case 13:
                                        _positions[_y, _x] = new Mirror(new Position() { X = _x, Y = _y }) { IsNotMovable = true };
                                        break;
                                    case 14:
                                        _positions[_y, _x] = new Mirror(new Position() { X = _x, Y = _y }) { IsNotMovable = true, Direction = MirrorDirection.Backward };
                                        break;
                                    case 15:
                                        _positions[_y, _x] = new Laser(new Position() { X = _x, Y = _y });
                                        break;
                                    case 17:
                                        _positions[_y, _x] = new Bomb(new Position() { X = _x, Y = _y });
                                        break;
                                    case 18:
                                        _positions[_y, _x] = new Laserball.Figures.Point(new Position() { X = _x, Y = _y });
                                        break;
                                    case 19:
                                        _positions[_y, _x] = new Border(new Position() { X = _x, Y = _y }) { Orientation = BoderOrientation.Center };
                                        break;
                                    case 20:
                                        _positions[_y, _x] = new Door(new Position() { X = _x, Y = _y });
                                        break;
                                    case 21:
                                        _positions[_y, _x] = new Door(new Position() { X = _x, Y = _y }) { DoorPosition = DoorStatus.Open };
                                        break;
                                    case 22:
                                        _positions[_y, _x] = new Door(new Position() { X = _x, Y = _y }) { Direction = DoorDirection.Vertical };
                                        break;
                                    case 23:
                                        _positions[_y, _x] = new Door(new Position() { X = _x, Y = _y }) { Direction = DoorDirection.Vertical, DoorPosition = DoorStatus.Open };
                                        break;
                                    case 24:
                                        _positions[_y, _x] = new Border(new Position() { X = _x, Y = _y }) { Orientation = BoderOrientation.BottomEnd };
                                        break;
                                    case 25:
                                        _positions[_y, _x] = new Border(new Position() { X = _x, Y = _y }) { Orientation = BoderOrientation.Vertical };
                                        break;
                                    case 26:
                                        _positions[_y, _x] = new Border(new Position() { X = _x, Y = _y }) { Orientation = BoderOrientation.TopEnd };
                                        break;
                                    case 27:
                                        _positions[_y, _x] = new Border(new Position() { X = _x, Y = _y }) { Orientation = BoderOrientation.LeftEnd };
                                        break;
                                    case 28:
                                        _positions[_y, _x] = new Border(new Position() { X = _x, Y = _y }) { Orientation = BoderOrientation.Horizontal };
                                        break;
                                    case 29:
                                        _positions[_y, _x] = new Border(new Position() { X = _x, Y = _y }) { Orientation = BoderOrientation.RightEnd };
                                        break;
                                    case 30:
                                        _positions[_y, _x] = new Border(new Position() { X = _x, Y = _y }) { Orientation = BoderOrientation.Round };
                                        break;
                                    case 31:
                                        _positions[_y, _x] = new Border(new Position() { X = _x, Y = _y }) { Orientation = BoderOrientation.TL };
                                        break;
                                    case 32:
                                        _positions[_y, _x] = new Border(new Position() { X = _x, Y = _y }) { Orientation = BoderOrientation.TR };
                                        break;
                                    case 33:
                                        _positions[_y, _x] = new Border(new Position() { X = _x, Y = _y }) { Orientation = BoderOrientation.BR };
                                        break;
                                    case 34:
                                        _positions[_y, _x] = new Border(new Position() { X = _x, Y = _y }) { Orientation = BoderOrientation.BL };
                                        break;
                                    case 35:
                                        _positions[_y, _x] = new Border(new Position() { X = _x, Y = _y }) { Orientation = BoderOrientation.Cross };
                                        break;
                                    case 40:
                                        _positions[_y, _x] = new MovableLR(new Position() { X = _x, Y = _y });
                                        break;
                                    case 41:
                                        _positions[_y, _x] = new MovableUD(new Position() { X = _x, Y = _y });
                                        break;
                                    case 42:
                                        _positions[_y, _x] = new Movable(new Position() { X = _x, Y = _y });
                                        break;
                                    default:
                                        throw new Exception("test");
                                        break;
                                }
                            }
                            catch (Exception)
                            {
                                //ignore
                            }
                            _x++;
                            if (_x == 10)
                            {
                                _x = 0;
                                _y++;
                            }
                        }
                        lineNumber++;
                    }
                }
            }

            json = JsonConvert.SerializeObject(_positions,
                new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    TypeNameHandling = TypeNameHandling.All
                });
            textBox1.Text = json;
        }
    }
}
