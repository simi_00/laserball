﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserballConsole.Figures
{
    public enum DoorDirection
    {
        Vertical, Horizontal
    }

    public enum DoorStatus
    {
        Open, Closed
    }
    public class Door:Figure
    {
        public DoorDirection Direction { get; set; }
        public DoorStatus DoorPosition { get; set; } = DoorStatus.Closed;
        public Door(Position pos) : base(pos)
        {
        }

        public override void ActivateLaser(Figure figure)
        {
                NeighborDirection input = GetDirection(figure);
                if (((input == NeighborDirection.Down || input == NeighborDirection.Up) && Direction == DoorDirection.Horizontal) || ((input == NeighborDirection.Right || input == NeighborDirection.Left) && Direction == DoorDirection.Vertical))
                {
                   DoorPosition = DoorStatus.Open; 
                }
            if ((((input == NeighborDirection.Left || input == NeighborDirection.Right) && Direction == DoorDirection.Horizontal) || ((input == NeighborDirection.Down || input == NeighborDirection.Up) && Direction == DoorDirection.Vertical))&& DoorPosition == DoorStatus.Open)
            {
                NeighborDirection output = GetOutputDirection(input);
                Neighbor nextNeighbor = GetNeighbor(output);
                if (nextNeighbor != null)
                {
                    Figure nextFigure = nextNeighbor.Fig;
                    nextFigure.ActivateLaser(this);
                }
                Status = FigureStatus.Active;
            }
        }
        public NeighborDirection GetOutputDirection(NeighborDirection inputDirection)
        {
            switch (inputDirection)
            {
                case NeighborDirection.Up:
                    return NeighborDirection.Down;
                case NeighborDirection.Right:
                    return NeighborDirection.Left;
                case NeighborDirection.Down:
                    return NeighborDirection.Up;
                case NeighborDirection.Left:
                    return NeighborDirection.Right;
                default:
                    throw new ArgumentOutOfRangeException(nameof(inputDirection), inputDirection, null);
            }
        }
        public override void Paint()
        {
            if (Status == FigureStatus.Active && DoorPosition == DoorStatus.Open)
            {
                Console.Write("|");
            }
            else if (Status == FigureStatus.Inactive && DoorPosition == DoorStatus.Open)
            {
                Console.Write("O");
            }
            else if (Status == FigureStatus.Inactive && DoorPosition == DoorStatus.Closed)
            {
                Console.Write("D");

            }
        }
    }
}
