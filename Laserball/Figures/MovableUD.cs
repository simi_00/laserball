﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laserball.Events;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Laserball.Figures
{
    public class MovableUD:Figure
    {
        private bool isActivated;
        public event EventHandler<MoveUDEvent> MoveUDEvent;
        public MovableUD(Position pos) : base(pos)
        {
            CanMove = true;
        }

        public override void ActivateLaser(Figure figure)
        {
            if (Input == NeighborDirection.Undefined)
            {
                Input = GetDirection(figure);
            }
            else
            {
                Input2 = GetDirection(figure);
            }
            if (!Board.IsMoving)
            {
                isActivated = true;
                MoveUDEvent args = new MoveUDEvent();
                args.MoveDirection = GetOutputDirection(Input);
                if (Input2 != NeighborDirection.Undefined)
                {
                    args.MoveDirection2 = GetOutputDirection(Input2);
                }
                OnMoveEvent(args);
            }
            Status = FigureStatus.Active;
        }
        public override void Paint(int posX, int posY, int height, SpriteBatch spriteBatch, ContentManager content)
        {
            Texture = content.Load<Texture2D>("movableUD");
            spriteBatch.Draw(Texture, new Rectangle(posX, posY, height, height), Color.White);
        }
        protected virtual void OnMoveEvent(MoveUDEvent e)
        {
            MoveUDEvent?.Invoke(this, e);
        }
    }
}
