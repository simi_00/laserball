﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LaserballConsole.Figures;

namespace LaserballConsole
{
    public class EmptyFigure:Figure
    {
        private NeighborDirection output;
        public EmptyFigure(Position pos) : base(pos)
        {

        }

        public override void ActivateLaser(Figure figure)
        {
            NeighborDirection input = GetDirection(figure);
            output = GetOutputDirection(input);
            Neighbor nextNeighbor = GetNeighbor(output);
            if (nextNeighbor != null)
            {
                Figure nextFigure = nextNeighbor.Fig;
                nextFigure.ActivateLaser(this);
            }
            Status = FigureStatus.Active;
        }

        public override void Paint()
        {
            if (output == NeighborDirection.Down || output == NeighborDirection.Up)
            {
                Console.Write(Status == FigureStatus.Active ? "|" : "*");
            }
            else
            {
                Console.Write(Status == FigureStatus.Active ? "-" : "*");
            }

        }

        public NeighborDirection GetOutputDirection(NeighborDirection inputDirection)
        {
            switch (inputDirection)
            {
                case NeighborDirection.Up:
                    return NeighborDirection.Down;
                case NeighborDirection.Right:
                    return NeighborDirection.Left;
                case NeighborDirection.Down:
                    return NeighborDirection.Up;
                case NeighborDirection.Left:
                    return NeighborDirection.Right;
                default:
                    throw new ArgumentOutOfRangeException(nameof(inputDirection), inputDirection, null);
            }
        }

    }
}
